local db = ...
print('database', db)

function post_save_filter(post)
    local userID = post:getOrigin():getUserIDString()
    print("Lua_post_filter: save " .. userID)
    return true
end

function user_save_filter(user)
    local user = user:getName()
    print("Lua_user_filter: save " .. user)
    return true
end