package edu.psu.hbg.turing.comp512.Config;

import edu.psu.hbg.turing.comp512.Communication.CommunicationError;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Identifier.*;
import edu.psu.hbg.turing.comp512.Platform.PlatformBase;
import edu.psu.hbg.turing.comp512.Platform.PlatformError;
import org.w3c.dom.*;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

/**
 * @author Anushruth
 * @version 0.1
 *          Configuration class. Handles reading from, and writing to config file. Most of the system parameters is stored in this singleton class
 *          all access is made using getters and setters.
 */
public class Configuration {

    public enum SystemType {
        OS_LINUX,
        OS_MAC,
        OS_BSD,
        OS_ANDROID,
        OS_WINDOWS
    }

    /**
     * Used to write and read tags in the configuration file. Instead of hardcoding the tag/attribute names they are put
     * in this enum and can be used by name() method to access the strings
     */
    private enum CONFIG_TAG {
        APPLICATION,
        CRYPTO,
        NODE,
        USER,
        GROUP,
        NAME,
        ID,
        CERTIFICATE,
        ALGORITHM,
        KEY_LENGTH,
        DIGEST,
        SIGNATURE,
        THREAD_POOL,
        PORT
    }

    private static SystemType host = null;
    private static Configuration instance = null;
    private static String configFile = null;
    private static Document config = null;
    private static edu.psu.hbg.turing.comp512.Identifier.Node nodeObject;


    /*
     *      =========================================================================
     *                          Initialization routines
     *      =========================================================================
     */

    /**
     * Initializes the configuration class
     *
     * @param configFile path to configuration xml file
     * @throws ConfigurationError Thrown for unsupported platform
     */
    public static void init(String configFile) throws ConfigurationError, PlatformError, CommunicationError {
        //Set Configuration file if only not set before
        Configuration.configFile = (Configuration.configFile == null) ? configFile : Configuration.configFile;

        if (host == null) {
            String hostPlatform = System.getProperty("os.name");
            Log.I("Starting SWSS on "+ hostPlatform);
            if (hostPlatform.toLowerCase().contains("linux")) {
                host = SystemType.OS_LINUX;
            } else if (hostPlatform.toLowerCase().contains("mac")) {
                host = SystemType.OS_MAC;
            } else if (hostPlatform.toLowerCase().contains("win")) {
                host = SystemType.OS_WINDOWS;
            }
            if (hostPlatform.toLowerCase().contains("bsd")) {
                host = SystemType.OS_BSD;
            }
            //TODO: Add more support. LOL
        }
        if (instance == null) {
            switch (host) {
                case OS_LINUX:
                case OS_MAC:
                case OS_BSD:
                    instance = new Configuration(configFile);
                    break;
                default:
                    instance = null;
                    throw new ConfigurationError("Unsupported Platform");
            }
        }
    }

    /**
     * Initializes the configuration class with a custom implementation of platform
     *
     * @param configFile Location of the config file
     * @param platform   Instance of platform implementation
     * @throws ConfigurationError
     * @throws PlatformError
     * @throws CommunicationError
     */
    public static void init(String configFile, PlatformBase.Platform platform) throws ConfigurationError, PlatformError, CommunicationError {
        PlatformBase.init(platform);
        init(configFile);
    }

    /*
     *      =========================================================================
     *                          End Initialization routines
     *      =========================================================================
     */


    /*
     * =========================================================================
     *                              Constructors
     * =========================================================================
     */
    private Configuration(String configFileOrDB) throws ConfigurationError, PlatformError, CommunicationError {
        try {
            File configFile = new File(configFileOrDB);
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            config = documentBuilder.parse(configFile);
            config.getDocumentElement().normalize();
        } catch (ParserConfigurationException ex) {
            throw new ConfigurationError(ex, "Parser Error " + ex.getMessage());
        } catch (SAXException ex) {
            throw new ConfigurationError(ex, ex.getMessage());
        } catch (IOException ex) {
            Log.E("Config not found, Creating new config");
            defaultConfig();
        }
    }

    /**
     * Creates new node identity and writes it down to the config file
     *
     * @param nodeName           Name of the node
     * @param nodeGroup          Group of the node
     * @param cipher             Cipher to be used
     * @param keyLength          Keylength for the cipher
     * @param signatureAlgorithm Signature algorithm to be used
     * @param digestAlgorithm    digest algorithm to be used
     * @param password           Password to encrypt the public certificate of the node
     * @return Node object created
     * @throws PlatformError
     * @throws ConfigurationError
     * @throws CommunicationError
     */
    public edu.psu.hbg.turing.comp512.Identifier.Node createNewNodeIdentity(String nodeName, String nodeGroup, String cipher, int keyLength, String signatureAlgorithm, String digestAlgorithm,
                                                                            char[] password) throws PlatformError, ConfigurationError, CommunicationError {
        try {
            MessageDigest digest = MessageDigest.getInstance(digestAlgorithm);
            byte[] nodeId = PlatformBase.getInstance().createNodeIdentity(cipher, keyLength, signatureAlgorithm, digestAlgorithm, nodeName == null ? "Node" : nodeName,
                    "Node", "None", "None", "None", password);
            digest.update(nodeId);
            edu.psu.hbg.turing.comp512.Identifier.Node node = edu.psu.hbg.turing.comp512.Identifier.Node.newNode(nodeName, nodeGroup, cipher,
                    keyLength, signatureAlgorithm, null, digestAlgorithm, digest.digest(), CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(nodeId)));
            addNodeID(node);
            return node;
        } catch (NoSuchAlgorithmException e) {
            throw new ConfigurationError(e, "Algorithm not available: " + e.getMessage());
        } catch (CertificateException e) {
            throw new ConfigurationError(e, "Error reading certificate " + e.getMessage());
        }
    }

    /**
     * Creates a new user and adds the user to the configuration file. It is suggested to use this method over the one
     * provided by the Platform class because this saves the user created to the config file. Hence making the access
     * to the user much easier. If created using the method in PlatformBase manual editing of config file is required
     * For the application to recognize the User.
     *
     * @param name          Name of the user
     * @param group         Group of the account
     * @param city          City of users residence
     * @param state         State of the users residence
     * @param country       Contry of users residence
     * @param cipher        Cipher to be used when creating the user account
     * @param digestAlgo    Digest algorithm to be used to hash the user account
     * @param signatureAlgo Signature algorithm to be used to sign the user certificate
     * @param keyLength     Key length for the cipher selected
     * @param password      Password to encrypt the user certificate with
     * @return User object thus created
     * @throws PlatformError
     * @throws ConfigurationError
     */
    public User createNewUser(String name, String group, String city, String state, String country, String cipher, String digestAlgo, String signatureAlgo, Integer keyLength,
                              char[] password) throws PlatformError, ConfigurationError {
        User user = PlatformBase.getInstance().createNewUser(name, group, city, state, country, cipher, digestAlgo, signatureAlgo, keyLength, password);
        addUserToConfig(user);
        return user;
    }

    /*
     *      =========================================================================
     *                              End Constructors
     *      =========================================================================
     */


    /*
     *      =========================================================================
     *                                  Getters
     *      =========================================================================
     */

    /**
     * Access method for Configuration class.
     *
     * @return Instance of configuration class or null
     */
    public static Configuration getInstance() {
        if (configFile == null || instance == null) {
            Log.E("Configuration not initialized");
            return null;
        }
        return instance;
    }


    /**
     * Get a list of strings containing name and id of the node profiles available on the machine
     *
     * @return A list of list of strings containing name and id
     */
    public String[][] getAvailableNodeConfigurations() {
        NodeList nodeList = config.getDocumentElement().getElementsByTagName(CONFIG_TAG.NODE.name());
        String[][] nodes = new String[nodeList.getLength()][2];
        for (int i = 0; i < nodeList.getLength(); i++) {
            nodes[i][0] = ((Element) nodeList.item(i)).getAttribute("Name");
            nodes[i][1] = ((Element) nodeList.item(i)).getAttribute("ID");
        }
        Log.I(nodeList.getLength() + " Node configurations found in config");
        return nodes;
    }

    /**
     * Get a list of strings containing user name and id of the profiles available on the machine
     *
     * @return A list of list of strings containing name and id
     */
    public String[][] getAvailableUsers() {
        NodeList nodeList = config.getDocumentElement().getElementsByTagName(CONFIG_TAG.USER.name());
        String[][] nodes = new String[nodeList.getLength()][2];
        for (int i = 0; i < nodeList.getLength(); i++) {
            nodes[i][0] = ((Element) nodeList.item(i)).getAttribute(CONFIG_TAG.NAME.name());
            nodes[i][1] = ((Element) nodeList.item(i)).getAttribute(CONFIG_TAG.ID.name());
        }
        Log.I(nodeList.getLength() + " User configurations found in config");
        return nodes;
    }

    /**
     * Loads the node parameters from the config file.
     *
     * @throws PlatformError      On certificate error
     * @throws ConfigurationError On incorrect configuration details
     * @throws CommunicationError On Error verifying the certificate
     */
    public edu.psu.hbg.turing.comp512.Identifier.Node loadNodeIdentity(String nodeName) throws PlatformError, ConfigurationError, CommunicationError {
        if (nodeName != null) {
            String name = getAttributeValue(nodeName, CONFIG_TAG.NODE, CONFIG_TAG.NAME);
            if(name != null) { //It is a node identity
                byte[] id = PlatformBase.getInstance().stringToByte(getAttributeValue(nodeName, CONFIG_TAG.NODE, CONFIG_TAG.ID));
                String digest = getAttributeValue(nodeName, CONFIG_TAG.NODE, CONFIG_TAG.DIGEST);
                String group = getAttributeValue(nodeName, CONFIG_TAG.NODE, CONFIG_TAG.GROUP);
                String algorithm = getAttributeValue(nodeName, CONFIG_TAG.NODE, CONFIG_TAG.ALGORITHM);
                String size = getAttributeValue(nodeName, CONFIG_TAG.NODE, CONFIG_TAG.KEY_LENGTH);
                int keySize;
                if (size != null) {
                    keySize = Integer.parseInt(size);
                } else
                    throw new ConfigurationError("Config file is corrupted, key size missing");
                String signatureAlgorithm = getAttributeValue(nodeName, CONFIG_TAG.NODE, CONFIG_TAG.SIGNATURE);
                Certificate cert = PlatformBase.getInstance().getCertificateFromCertificateString(getAttributeValue(nodeName, CONFIG_TAG.NODE, CONFIG_TAG.CERTIFICATE));
                Log.I("Attempting to Load node " + nodeName);
                //InetAddress refers to the address of this node. Hence left to null
                return edu.psu.hbg.turing.comp512.Identifier.Node.newNode(name, group, algorithm, keySize, signatureAlgorithm, null, digest, id, cert);
            }else {
                //User identity is used as node
                name = getAttributeValue(nodeName, CONFIG_TAG.USER, CONFIG_TAG.NAME);
                if(name == null){
                    return null;
                }
                byte[] id = PlatformBase.getInstance().stringToByte(getAttributeValue(nodeName, CONFIG_TAG.USER, CONFIG_TAG.ID));
                String digest = getAttributeValue(nodeName, CONFIG_TAG.USER, CONFIG_TAG.DIGEST);
                String group = getAttributeValue(nodeName, CONFIG_TAG.USER, CONFIG_TAG.GROUP);
                String algorithm = getAttributeValue(nodeName, CONFIG_TAG.USER, CONFIG_TAG.ALGORITHM);
                String size = getAttributeValue(nodeName, CONFIG_TAG.USER, CONFIG_TAG.KEY_LENGTH);
                int keySize = 1024;
//                if (size != null) {
//                    keySize = Integer.parseInt(size);
//                } else
//                    throw new ConfigurationError("Config file is corrupted, key size missing");
                String signatureAlgorithm = getAttributeValue(nodeName, CONFIG_TAG.NODE, CONFIG_TAG.SIGNATURE);
                Certificate cert = PlatformBase.getInstance().getCertificateFromCertificateString(getAttributeValue(nodeName, CONFIG_TAG.USER, CONFIG_TAG.CERTIFICATE));
                Log.I(cert.getPublicKey().getAlgorithm());
                Log.I("Attempting to load node " + nodeName);
                return edu.psu.hbg.turing.comp512.Identifier.Node.newNode(name, group, algorithm, keySize, signatureAlgorithm, null, digest, id, cert);
            }
        } else throw new ConfigurationError("Passed in a null node name??");
    }

    /**
     * Returns the type of system node is running on
     *
     * @return Enum of System type describing the type of system running on
     * @throws ConfigurationError
     */
    public static SystemType getHost() throws ConfigurationError {
        if (host != null) {
            return host;
        } else throw new ConfigurationError("Host not determined/supported");
    }

    /**
     * Load user by name from config file and return back the object
     *
     * @param name User name to be loaded. In case of multiple entries with same name first one is selected
     * @return User object corresponding to the name given
     * @throws PlatformError
     * @throws ConfigurationError
     */
    public User loadUser(String name) throws PlatformError, ConfigurationError {
        String group = getAttributeValue(name, CONFIG_TAG.USER, CONFIG_TAG.GROUP);
        String id = getAttributeValue(name, CONFIG_TAG.USER, CONFIG_TAG.ID);
        String digest = getAttributeValue(name, CONFIG_TAG.USER, CONFIG_TAG.DIGEST);
        String signature = getAttributeValue(name, CONFIG_TAG.USER, CONFIG_TAG.SIGNATURE);
        String certificate = getAttributeValue(name, CONFIG_TAG.USER, CONFIG_TAG.CERTIFICATE);
        return new User(name, group, PlatformBase.getInstance().stringToByte(id), digest, signature, PlatformBase.getInstance().getCertificateFromCertificateString(certificate));
    }


    /**
     * Returns node object for the present setup
     *
     * @return Node object for this node
     */
    public edu.psu.hbg.turing.comp512.Identifier.Node getNodeObject() {
        return nodeObject;
    }


    /**
     * Returns the thread pool size as configured in the config file
     * @return  thread pool size configured or default value of 20.
     */
    public int getThreadPoolSize() {
        String poolSize = getAttributeValue(CONFIG_TAG.APPLICATION, CONFIG_TAG.THREAD_POOL);
        if(poolSize != null){
            return Integer.parseInt(poolSize);
        }
        Log.E("Did not find an entry for pool size in the config file using default value of 20");
        return 20;
    }

    /**
     * Returns port configured for starting the server on
     * @return  integer containing the port number to be used
     */
    public int getServerPort() {
        String port = getAttributeValue(CONFIG_TAG.APPLICATION, CONFIG_TAG.PORT);
        if(port != null){
            return Integer.parseInt(port);
        }
        Log.E("Did not find an entry for pool size in the config file using default value of 20");
        return 20;
    }


    /**
     * Get attribute value of the first entry in the config file
     *
     * @param tagName   Type of the entry
     * @param attribute Name of the entry
     * @return String containing the value of the attribute or null if not found
     */
    private String getAttributeValue(CONFIG_TAG tagName, CONFIG_TAG attribute) {
        NodeList nodeList = config.getDocumentElement().getElementsByTagName(tagName.name());
        Node node;
        if (nodeList != null && nodeList.getLength() > 0) {
            node = nodeList.item(0);
            return ((Element) node).getAttribute(attribute.name());
        }
        Log.E(tagName + " : " + attribute + "Not found");
        return null;
    }

    /**
     * Get value of the attribute with the corresponding name
     *
     * @param name      Name of the entry
     * @param tagName   Type of the entry
     * @param attribute Name of the attribute
     * @return String containing the attribute value
     */
    private String getAttributeValue(String name, CONFIG_TAG tagName, CONFIG_TAG attribute) {
        NodeList nodeList = config.getDocumentElement().getElementsByTagName(tagName.name());
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (((Element) node).getAttribute(CONFIG_TAG.NAME.name()).equals(name))
                return ((Element) node).getAttribute(attribute.name());
        }
        return null;
    }

    /**
     * Returns the name of the application that is presently using the library
     *
     * @return String containing the name of the application
     */
    public String getApplicationName() {
        return getAttributeValue(CONFIG_TAG.APPLICATION, CONFIG_TAG.NAME);
    }

    /*
     *      =========================================================================
     *                              End Getters
     *      =========================================================================
     */


    /*
     *      =========================================================================
     *                                  Setters
     *      =========================================================================
     */

    /**
     * Set the identity of the created node
     *
     * @param node Node object containing identity of the created node
     */
    public void setNodeIdentity(edu.psu.hbg.turing.comp512.Identifier.Node node) {
        nodeObject = node;
        Log.I("Node Identity set to " + node.getName() + " : " + node.getIDString());
    }


    /**
     * Create a default configuration file and save it in the specified location
     */
    private void defaultConfig() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            config = docBuilder.newDocument();
            Element root = config.createElement("SWSS");
            config.appendChild(root);
            Element application = config.createElement(CONFIG_TAG.APPLICATION.name());
            root.appendChild(application);
            Attr appName = config.createAttribute(CONFIG_TAG.NAME.name());
            appName.setValue("SWSS");
            application.setAttributeNode(appName);
            Attr threadPoolSize = config.createAttribute(CONFIG_TAG.THREAD_POOL.name());
            threadPoolSize.setValue("10");
            application.setAttributeNode(threadPoolSize);
            Attr listeningPort = config.createAttribute(CONFIG_TAG.PORT.name());
            listeningPort.setValue("8080");
            application.setAttributeNode(listeningPort);
            saveConfigFile();
        } catch (ParserConfigurationException e) {
            Log.E("Error parsing config " + e.getMessage());
        } catch (ConfigurationError configurationError) {
            Log.E("Error configuring default settings " + configurationError.getMessage());
        }
    }

    /**
     * Method to save the configuration file in the specified location
     *
     * @throws ConfigurationError
     */
    private void saveConfigFile() throws ConfigurationError {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(config);
            StreamResult result = new StreamResult(new File(configFile));
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            throw new ConfigurationError(ex, "Unable to save config file: " + ex.getMessage());
        }
    }

    /**
     * Adds user data to the configuration file and then saves the file to the hard disk
     *
     * @param user User object to be written
     * @throws ConfigurationError
     * @throws PlatformError
     */
    private void addUserToConfig(User user) throws ConfigurationError, PlatformError {
        Element root = config.getDocumentElement();
        Element userElement = config.createElement(CONFIG_TAG.USER.name());
        root.appendChild(userElement);
        Attr name = config.createAttribute(CONFIG_TAG.NAME.name());
        name.setValue(user.getName());
        Attr id = config.createAttribute(CONFIG_TAG.ID.name());
        id.setValue(user.getIDString());
        Attr digest = config.createAttribute(CONFIG_TAG.DIGEST.name());
        digest.setValue(user.getDigestAlgo());
        Attr signature = config.createAttribute(CONFIG_TAG.SIGNATURE.name());
        signature.setValue(user.getSignatureAlgo());
        Attr certificate = config.createAttribute(CONFIG_TAG.CERTIFICATE.name());
        certificate.setValue(user.getCertificateString());
        Attr group = config.createAttribute(CONFIG_TAG.GROUP.name());
        group.setValue(user.getGroup());
        userElement.setAttributeNode(name);
        userElement.setAttributeNode(id);
        userElement.setAttributeNode(digest);
        userElement.setAttributeNode(signature);
        userElement.setAttributeNode(certificate);
        userElement.setAttributeNode(group);
        saveConfigFile();
    }


    /**
     * Add the node ID to the configuration file. This is used by create node id to store info about node into the config file
     * @param nodeObj               Node to be saved to the memory
     * @throws ConfigurationError
     * @throws PlatformError
     */
    private void addNodeID(edu.psu.hbg.turing.comp512.Identifier.Node nodeObj) throws ConfigurationError, PlatformError {
        if(nodeObj != null) {
            Element doc = config.getDocumentElement();
            Element node = config.createElement(CONFIG_TAG.NODE.name());
            doc.appendChild(node);
            Attr id = config.createAttribute(CONFIG_TAG.ID.name());
            id.setValue(nodeObj.getIDString());
            Attr nodeName = config.createAttribute(CONFIG_TAG.NAME.name());
            nodeName.setValue(nodeObj.getName());
            Attr nodeGroup = config.createAttribute(CONFIG_TAG.GROUP.name());
            nodeGroup.setValue(nodeObj.getGroup());
            Attr nodeAlgorithm = config.createAttribute(CONFIG_TAG.ALGORITHM.name());
            nodeAlgorithm.setValue(nodeObj.getAlgorithm());
            Attr keyLength = config.createAttribute(CONFIG_TAG.KEY_LENGTH.name());
            keyLength.setValue(String.valueOf(nodeObj.getKeyLength()));
            Attr digest = config.createAttribute(CONFIG_TAG.DIGEST.name());
            digest.setValue(nodeObj.getDigestAlgo());
            Attr signature = config.createAttribute(CONFIG_TAG.SIGNATURE.name());
            signature.setValue(nodeObj.getSignatureAlgorithm());
            Attr certificate = config.createAttribute(CONFIG_TAG.CERTIFICATE.name());
            certificate.setValue(PlatformBase.getInstance().getCertificateStringFromCertificate(nodeObj.getCertificate()));
            node.setAttributeNode(nodeGroup);
            node.setAttributeNode(nodeAlgorithm);
            node.setAttributeNode(keyLength);
            node.setAttributeNode(digest);
            node.setAttributeNode(signature);
            node.setAttributeNode(id);
            node.setAttributeNode(nodeName);
            node.setAttributeNode(certificate);
            saveConfigFile();
        }else{
            throw new ConfigurationError("Null object cannot be saved to memory");
        }
    }

    /*
     *      =========================================================================
     *                                  End Setters
     *      =========================================================================
     */
}
