package edu.psu.hbg.turing.comp512.Config;

/**
 * Created by regnarts on 3/9/17.
 */
public class ConfigurationError extends Exception {
    public ConfigurationError(String msg){
        //TODO: Put correct exception here
        super(msg);
    }
    public ConfigurationError(Exception ex, String message){ super(message, ex);}
}
