package edu.psu.hbg.turing.comp512.Log;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * @author Anushruth
 * @version 0.1
 */
public class Log {

    private static PrintStream logFile = System.out;

    public static void SetFile(String filePath){
        try {
            logFile = new PrintStream(new FileOutputStream(filePath));
        } catch (FileNotFoundException e) {
            logFile = System.out;
            Log.E("Unable to save logs to "+filePath+" "+ e.getMessage());
        }
    }

    public static void I(String message){
        synchronized (logFile) {
            StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
            logFile.println("INFO: " + stackTraceElement[2].getClassName() + " [" + stackTraceElement[2].getMethodName() + "] (" + stackTraceElement[2].getLineNumber() + ")" + ": " + message);
        }
    }

    public static void I(String message, Exception e){
        synchronized (logFile) {
            StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
            logFile.println("INFO: " + stackTraceElement[2].getClassName() + " [" + stackTraceElement[2].getMethodName() + "] (" + stackTraceElement[2].getLineNumber() + ")" + ": " + message+" ["+e.getMessage()+"]");
        }
    }

    public static void E(String message){
        synchronized (logFile) {
            StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
            logFile.println("ERRO: " + stackTraceElement[2].getClassName() + " [" + stackTraceElement[2].getMethodName() + "] (" + stackTraceElement[2].getLineNumber() + ")" + ": " + message);
        }
    }

    public static void E(String message, Exception e){
        synchronized (logFile) {
            StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
            logFile.println("ERRO: " + stackTraceElement[2].getClassName() + " [" + stackTraceElement[2].getMethodName() + "] (" + stackTraceElement[2].getLineNumber() + ")" + ": " + message+" ["+e.getMessage()+"]");
        }
    }

    public static void W(String message){
        synchronized (logFile) {
            StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
            logFile.println("WARN: " + stackTraceElement[2].getClassName() + " [" + stackTraceElement[2].getMethodName() + "] (" + stackTraceElement[2].getLineNumber() + ")" + ": " + message);
        }
    }

    public static void W(String message, Exception e){
        synchronized (logFile) {
            StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
            logFile.println("WARN: " + stackTraceElement[2].getClassName() + " [" + stackTraceElement[2].getMethodName() + "] (" + stackTraceElement[2].getLineNumber() + ")" + ": " + message+" ["+e.getMessage()+"]");
        }
    }
}
