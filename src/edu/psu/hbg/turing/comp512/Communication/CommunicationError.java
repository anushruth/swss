package edu.psu.hbg.turing.comp512.Communication;

/**
 * Created by regnarts on 3/22/17.
 */
public class CommunicationError extends Exception {
    public CommunicationError(String msg){
        super(msg);
    }
    public CommunicationError(Exception ex, String message){ super(message, ex);}
}
