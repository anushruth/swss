package edu.psu.hbg.turing.comp512.Communication;

import edu.psu.hbg.turing.comp512.Config.Configuration;
import edu.psu.hbg.turing.comp512.DataBase.DataBaseManager;
import edu.psu.hbg.turing.comp512.DataBase.Message;
import edu.psu.hbg.turing.comp512.DataBase.Post;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Identifier.Node;
import edu.psu.hbg.turing.comp512.Identifier.User;
import edu.psu.hbg.turing.comp512.Platform.PlatformBase;
import edu.psu.hbg.turing.comp512.Utils.XORComparator;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author anushruth
 * @version 0.1
 *          <p>The Comms class is an interface to all the communication funtions</p>
 */
public class Comms {
    private static Comms instance = null;
    private static ServerSocket server = null;
    private static int socketPort = 0;
    private static boolean serverRunning = false;
    private static Map<String, Node> commsMap = null;
    private static Thread serverThread = null;

    //Put this in for testing purposes. Replace with standard size later
    private static final int  commsMapSize = 15;

    /**
     * Initialize the Comms module with the port for the server
     *
     * @param socketPort The number of the port
     */
    public static void init(int socketPort) {
        Comms.socketPort = socketPort;
    }

    /**
     * Returns the instance of the Comms module.
     *
     * @return Instance of the Comms module
     */
    public static Comms getInstance() {
        if (instance == null)
            try {
                instance = new Comms(socketPort);
            } catch (CommunicationError communicationError) {
                Log.I(communicationError.getMessage());
                return null;
            }
        return instance;
    }

    /**
     * Constructor with port initialized to it.
     *
     * @param port the port number to start the server on
     * @throws CommunicationError
     */
    public Comms(int port) throws CommunicationError {
        try {
            if(socketPort==0 && port!=0 )
                socketPort = port;
            else
                socketPort=Configuration.getInstance().getServerPort();
            commsMap = new HashMap<>();
            commsMap.put("3FFDB53FBC7A444D797037A0DB6E0E3DE357940A80AAA516185C452946381FAA", Node.newNode("minerva", "3FFDB53FBC7A444D797037A0DB6E0E3DE357940A80AAA516185C452946381FAA", InetAddress.getByName("malumdomum.duckdns.org"), 8080));
            commsMap.put("2D6B83223A68C694CAA5AF7B5C4E18F3591560C8156C56A788E7F6ABDB4C25BC", Node.newNode("athena", "C23406A1C1C5D7BE21BA621DA637BBD5BB4E89403859148F6935F2856D4AB016", InetAddress.getByName("ec2-52-42-200-169.us-west-2.compute.amazonaws.com"), 8080));
        } catch (IOException e) {
            throw new CommunicationError(e, "Failed to create Server: " + e.getMessage());
        }
    }

    /**
     * Update the cache of nodes
     *
     * @param link Node that needs to be updated
     */
    private void updateLink(Node link) {
        if (link != null && commsMap != null && !link.getIDString().equalsIgnoreCase(Configuration.getInstance().getNodeObject().getIDString())) {
            synchronized (commsMap) {
                if (link.getIDString() != null) {
                    if (commsMap.containsKey(link.getIDString())) {
                        commsMap.replace(link.getIDString(), link);
                    } else {
                        if(commsMap.size()>commsMapSize) {
                            Node hostNode = Configuration.getInstance().getNodeObject();
                            PriorityQueue<Node> nodeQueue = new PriorityQueue<>(new XORComparator(hostNode).reverse());
                            nodeQueue.addAll(commsMap.values());
                            Node removeCandidate = nodeQueue.remove();
                            if(XORComparator.getDistanceBetween(hostNode.getID(), removeCandidate.getID()) > XORComparator.getDistanceBetween(hostNode.getID(),link.getID())) {
                                commsMap.remove(removeCandidate.getIDString());
                                commsMap.put(link.getIDString(),link);
                            }
                        }
                        else
                            commsMap.put(link.getIDString(),link);
                    }
                }
            }
        }
    }

    /**
     * Get the nodes near to the given by the ID
     *
     * @param ID    The target
     * @param limit Limit in the number of nodes that will be returned
     * @return List containing the node objects
     */
    private List<Node> getNodesNear(String ID, int limit) {
        if (ID != null) {
            PriorityQueue<Node> nodePriorityQueue = new PriorityQueue<Node>(new XORComparator(PlatformBase.getInstance().stringToByte(ID)));
            synchronized (commsMap) {
                nodePriorityQueue.addAll(commsMap.values());
            }
            List<Node> nodeList = new ArrayList<>();
            for (int i = 0; i < limit && !nodePriorityQueue.isEmpty(); i++) {
                nodeList.add(nodePriorityQueue.remove());
            }
            return nodeList;
        }
        Log.E("Empty String input");
        return null;
    }

    /**
     * Proceses the request or update message that comes to the node
     *
     * @param client socket of the client that is trying to communicate
     */
    private void processClient(Socket client) {
        try {
            Message msg = extractMessage(client);
            Log.I("Updating client "+msg.getOrigin().getName());
            updateLink(msg.getOrigin());
            if (msg.getNodes() != null) {
                for (Node link : msg.getNodes()) {
                    updateLink(link);
                }
            }
            switch (msg.getType()) {
                case MESSAGE_REQ_UPDATE:
                    respondToUpdateRequest(client, msg);
                    break;
                case MESSAGE_UPDATE:
                    //Nothing to do here. Database is already updated. Bad design? !!
                    break;
//                case MESSAGE_REQ_POST:
//                    respondToPostReq(client, msg);
//                    break;
                case MESSAGE_REQ_NODES:
                    respondWithNodeList(client, msg);
                    break;
            }
            client.close();
        } catch (IOException e) {
            Log.I("Error getting input stream for " + client.getInetAddress().getHostAddress());
            return;
        } catch (CommunicationError communicationError) {
            Log.W("Error Parsing message from " + client.getInetAddress().getHostAddress() + " : " + communicationError.getMessage());
        }
    }

    /**
     * Send message with nodelist with smallest distance from requested user if asked. Else send random nodes
     *
     * @param client Client requesting for nodes
     * @param msg    Message sent by the client
     */
    private void respondWithNodeList(Socket client, Message msg) {
        List<Node> nodes = new ArrayList<>();
        if (msg.getUserInfoReq() != null) {
            for (String id : msg.getUserInfoReq()) {
                List<Node> nl = getNodesNear(id, msg.getLimit() == null ? 10 : msg.getLimit());
                if (nl != null)
                    nodes.addAll(nl);
            }
        } else {
            synchronized (commsMap) {
                commsMap.forEach((id, node) -> {
                    nodes.add(node); //add code to use limits
                });
            }
        }
        Log.I("Sending " + nodes.size() + " nodes");
        Message message = new Message(Message.MessageType.MESSAGE_UPDATE, null, null, nodes,null, null, null,null);
        if (message != null)
            sendMessage(message, client);
    }

    /**
     * Respon to a update request. Depending on the request
     *
     * @param client socket of the node that requested the update
     * @param msg    An object of type message
     */
    private void respondToUpdateRequest(Socket client, Message msg) {
        List<User> userList = null;
        List<Post> posts = null;
        List<Node> nodes = null;
        if (msg.getUserInfoReq() != null) {
            userList = new ArrayList<>(msg.getUserInfoReq().size());
            for (String userID : msg.getUserInfoReq()) {
                User usr = DataBaseManager.getInstance().getUser(userID);
                if (usr != null) {
                    userList.add(usr);
                }
            }
        }
        if (msg.getPostReq() != null) {
            posts = new ArrayList<>(msg.getPostReq().size());
            for (String postID : msg.getPostReq()) {
                Post pst = DataBaseManager.getInstance().getPost(postID);
                if (pst != null) {
                    posts.add(pst);
                }
            }
        }
        if(msg.getNodeInfoReq() != null){
            nodes = new ArrayList<>();
            PriorityQueue<Node> nodeQueue = new PriorityQueue<>(new XORComparator(msg.getOrigin().getID()));
            synchronized (commsMap){
                nodeQueue.addAll(commsMap.values());
                while(!nodeQueue.isEmpty()){
                    nodes.add(nodeQueue.remove());
                }
            }
        }
        if (((userList != null) && (userList.size() > 0)) || ((posts != null) && (posts.size() > 0)) || (nodes!=null)&&(nodes.size()>0)) {
            Message reply = new Message(Message.MessageType.MESSAGE_UPDATE, userList, posts, nodes, null, null, null,null);
            sendMessage(reply, client);
        } else {
            posts = DataBaseManager.getInstance().getRecentPosts(msg.getLimit() == null ? 100 : msg.getLimit());
            List<User> userList1 = new ArrayList<User>();
            for (Post post : posts) {
                User usr = post.getOrigin();
                if (usr != null)
                    userList1.add(usr);
            }
            Message reply = new Message(Message.MessageType.MESSAGE_UPDATE, userList1, posts, null, null, null, null,null);
            sendMessage(reply, client);
        }
    }

    /**
     * Extracts message from the socket and converts it into Message class.
     *
     * @param client Socket object of the client
     * @return
     * @throws CommunicationError
     * @throws IOException
     */
    private Message extractMessage(Socket client) throws CommunicationError, IOException {
        byte[] buffer = new byte[40960];
        int size = client.getInputStream().read(buffer);
        Scanner input = new Scanner(new ByteArrayInputStream(buffer));
        String signature = input.nextLine();
        if (signature.equals("swss")) {
//            size;
            String msgSizeString = input.nextLine();
//            size += msgSizeString.length();
            int msgSize = Integer.parseInt(msgSizeString);
            msgSize+=msgSizeString.length()+7;
            if (msgSize < 40960) {
                while(msgSize!=size){
                    size += client.getInputStream().read(buffer,size,buffer.length-size);
                    input = new Scanner(new ByteArrayInputStream(buffer));
                    input.nextLine();
                    input.nextLine();
                }
                Log.I("Size "+ size+" messageSize "+msgSize+" diff "+(msgSize-size));
                String inputString = null;
                try {
                    Log.I("Constructing Message object");
                    inputString = input.nextLine();
//                    inputString = inputString.substring(0,inputString.length()-1);
                    JSONObject msg = new JSONObject(inputString);
                    return new Message(msg, client);
                }catch (JSONException ex){
                    Log.E("Incomplete message from "+client.getInetAddress().getHostName()+inputString, ex);
                    client.close();
                    return null;
                }
            } else {
                Log.I("Message is very big !! " + msgSize);
                return null;
            }
        } else {
            throw new CommunicationError("Unrecognized Message signature " + signature);
        }
    }

    /**
     * Sends message to the required reciever or ends up sending to the nearest 10 of the nodes
     *
     * @param msg      Message object
     * @param receiver Socket of the reciever
     * @return
     */
    public boolean sendMessage(Message msg, String receiver) {
        PriorityQueue<Node> queue = new PriorityQueue<Node>(new XORComparator(PlatformBase.getInstance().stringToByte(receiver)));
        queue.addAll(commsMap.values());
        int i = 10;
        boolean ret = false;
        while (!queue.isEmpty() && i != 0 ) {
            Node link = queue.remove();
            if (link != null && !Configuration.getInstance().getNodeObject().getIDString().equalsIgnoreCase(link.getIDString())) {
                try {
                    Socket socket = new Socket(link.getNodeAddress(), link.getPort());
                    if (sendMessage(msg, socket))
                        ret = true;
                } catch (IOException e) {
                    Log.W(receiver + " not found " + e.getMessage());
                }
            } else {
                Log.W("Unable to send message to " + receiver + ", no link found");
            }
        }
        return ret;
    }

    /**
     * Sends message to user and all the nodes near the user
     * @param msg   Message object
     * @param user  User object
     * @return  true on
     */
    public boolean sendMessage(Message msg, User user) {
        PriorityQueue<Node> queue = new PriorityQueue<Node>(new XORComparator(user));
        queue.addAll(commsMap.values());
        int i = commsMapSize;
        boolean ret = false;
        while (!queue.isEmpty() && i != 0) {
            Node link = queue.remove();
            if (link != null && !link.getIDString().equalsIgnoreCase(Configuration.getInstance().getNodeObject().getIDString())) {
                try {
                    Socket socket = new Socket(link.getNodeAddress(), link.getPort());
                    if (sendMessage(msg, socket))
                        ret = true;
                } catch (IOException e) {
                    Log.W(link.getName()+ " not found " + e.getMessage());
                }
            } else {
                Log.W("Unable to send message to " + user.getName() + ", no link found");
            }
        }
        return ret;
    }

    /**
     * Send a message to the receiver
     *
     * @param msg      message object
     * @param receiver socket of the receiver
     * @return true if the message was successfully sent false otherwise
     */
    public boolean sendMessage(Message msg, Socket receiver) {
        if (receiver != null) {
            msg.build();
            try {
                PrintWriter pw = new PrintWriter(receiver.getOutputStream());
                pw.write("swss\n".toCharArray());
                pw.print(msg.getMessageSize() + "\n");
                pw.print(msg.getMessage() + "\n");
                pw.flush();
                Log.I("Sent message of size " + msg.getMessageSize());
                return true;
            } catch (IOException e) {
                Log.W("Unable to send message to " + receiver.getInetAddress().getHostName());
                return false;
            }
        } else {
            Log.E("Receiver is null ??!!");
            return false;
        }
    }

    /**
     * Send a message and wait for reply. If the exact node is not available the message is sent the nearest commsMapSize nodes
     * until a reply is found
     *
     * @param msg      message object
     * @param receiver socket of the intended recipient
     */
    public Message sendMessageWaitForReply(Message msg, String receiver) {
        synchronized (commsMap) {
            PriorityQueue<Node> queue = new PriorityQueue<>(new XORComparator(PlatformBase.getInstance().stringToByte(receiver)));
            queue.addAll(commsMap.values());
            int i = commsMapSize;
            while (!queue.isEmpty() && i > 0) {
                try {
                    Node link = queue.remove();
                    if(!link.getIDString().equalsIgnoreCase(Configuration.getInstance().getNodeObject().getIDString())) {
                        Socket socket = new Socket(link.getNodeAddress(), link.getPort());
                        return sendMessageWaitForReply(msg, socket);
                    }
                } catch (IOException e) {
                    Log.W(receiver + " not found " + e.getMessage());
                }
                i--;
            }
        }
        return null;
    }

    /**
     * Send a message and wait for reply.
     *
     * @param msg      message object
     * @param receiver node object of receiver
     * @return Message response from node or null
     */
    public Message sendMessageWaitForReply(Message msg, Node receiver) {
        if(msg!=null&&receiver!=null&&!receiver.getIDString().equalsIgnoreCase(Configuration.getInstance().getNodeObject().getIDString())) {
            synchronized (commsMap) {
                try {

                    Socket socket = new Socket(receiver.getNodeAddress(), receiver.getPort());
                    return sendMessageWaitForReply(msg, socket);
                } catch (IOException e) {
                    Log.W(receiver + " not found " + e.getMessage());
                    return null;
                }
            }
        }
        Log.E("Null or invalid inputs to the function!!");
        return null;
    }

    /**
     * Send a message to nearest spray size of nodes and wait for reply.
     *
     * @param msg      message object
     * @param receiver socket of the intended recipient
     * @param spraySize max number of nodes to query from
     * @return List of messages received as response
     */
    public List<Message> sendMessageWaitForReply(Message msg, String receiver, int spraySize) {
        synchronized (commsMap) {
            PriorityQueue<Node> queue = new PriorityQueue<>(new XORComparator(PlatformBase.getInstance().stringToByte(receiver)));
            queue.addAll(commsMap.values());
            int i = spraySize;
            List<Message> replies = new ArrayList<>(spraySize);
            while (!queue.isEmpty() && i > 0) {
                try {
                    Node link = queue.remove();
                    if(!link.getIDString().equalsIgnoreCase(Configuration.getInstance().getNodeObject().getIDString())) {
                        Socket socket = new Socket(link.getNodeAddress(), link.getPort());
                        Message reply = sendMessageWaitForReply(msg, socket);
                        if (reply != null)
                            replies.add(reply);
                    }
                } catch (IOException e) {
                    Log.W(receiver + " not found " + e.getMessage());
                }
                i--;
            }
            if(replies.size()>0)
                return replies;
            Log.W("Couldn't find a single reply for "+receiver);
            return null;
        }
    }

    public void updateNodes(){
        synchronized (commsMap){
            List<Node> nodeList = new ArrayList<>();
            commsMap.values().forEach((node -> {
                Message reply = sendMessageWaitForReply(new Message(Message.MessageType.MESSAGE_REQ_NODES,null,null,null,null,null,null,null),node.getIDString());
                if((reply!=null) && (reply.getNodes()!=null)){
                    nodeList.addAll(reply.getNodes());
                    Log.I("Found "+reply.getNodes().size()+" nodes");
                }
            }));
            nodeList.forEach(node -> {updateLink(node); Log.I("Adding "+node.getName()+" : "+node.getNodeAddress().getHostName());});
        }
    }

    public Node findNode(String ID) {
        Log.I("Trying to find " + ID);
        if (Configuration.getInstance().getNodeObject().getIDString().equalsIgnoreCase(ID))
            return Configuration.getInstance().getNodeObject();
        Node node;
        synchronized (commsMap) {
            node = commsMap.get(ID);
        }
        int i = 35;
        if (node == null) {
            PriorityQueue<Node> queue = new PriorityQueue<>(new XORComparator(PlatformBase.getInstance().stringToByte(ID)));
            synchronized (commsMap) {
                queue.addAll(commsMap.values());
            }
            Set<String> visitedNodes = new HashSet<String>();
            while (!queue.isEmpty() && i > 0) {
                Node link = queue.remove();
                if(visitedNodes.contains(link.getIDString())) {
                    continue;
                }
                try {
                    Log.I(Configuration.getInstance().getNodeObject().getName() + ": Asking " + link.getName() + " who is " + ID);
                    Socket socket = new Socket(link.getNodeAddress(), link.getPort());
                    List<String> nodes = new ArrayList<>(1);
                    nodes.add(ID);
                    Message reply = sendMessageWaitForReply(new Message(Message.MessageType.MESSAGE_REQ_UPDATE, null, null, null, null, null, nodes, commsMapSize), socket);
                    if (reply != null && reply.getType() == Message.MessageType.MESSAGE_UPDATE) {
                        reply.getNodes().stream().forEach((nd) -> {
                            if (!visitedNodes.contains(nd.getIDString())) {
                                queue.add(nd);
                            }
                        });
                        for (Node nodeUpdate : reply.getNodes()) {
                            if (nodeUpdate.getIDString().equalsIgnoreCase(ID)) {
                                Log.I("Found " + nodeUpdate.getName() + " from " + Configuration.getInstance().getNodeObject().getName()
                                        + "[" + XORComparator.getDistanceBetween(Configuration.getInstance().getNodeObject().getID(),
                                        nodeUpdate.getID()) + "] in " + (35 - i) + " hops ");
                                return nodeUpdate;
                            }
                            updateLink(node);
                        }
                        i--;
                    }
                    visitedNodes.add(link.getIDString());
                } catch (IOException e) {
                    Log.W(link.getName() + " not found " + e.getMessage());
                }
            }
        }else
            return node;
        Log.I("Failed to find " + ID + " from " + Configuration.getInstance().getNodeObject().getName() + " [" +
                Math.abs(XORComparator.getDistanceBetween(Configuration.getInstance().getNodeObject().getID(),
                        PlatformBase.getInstance().stringToByte(ID))) + "]" + " in "+ (35-i)+" hops");
        return null;

    }

    /**
     * Same as the above. Except that the socket is input. So message is only sent to the intended recipient.
     *
     * @param msg      Object containing the message
     * @param receiver socket of the intended recipient
     * @return The message that will be replied
     */
    public Message sendMessageWaitForReply(Message msg, Socket receiver) {
        try {
            receiver.setSoTimeout(15000);
            sendMessage(msg, receiver);
            return extractMessage(receiver);
        } catch (SocketException e) {
            Log.W("Unable to receive reply from " + receiver.getInetAddress().getHostAddress() + " " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.W("Unable to receive reply from " + receiver.getInetAddress().getHostAddress() + " " + e.getMessage());
            return null;
        } catch (CommunicationError communicationError) {
            Log.W("Unable to receive reply from " + receiver.getInetAddress().getHostAddress() + " " + communicationError.getMessage());
            return null;
        }
    }

    /**
     * Starts the server thread
     * TODO: add code to connect to a few nodes and get mapping of other nodes on start
     */
    public void startServer() {
        try {
            server = new ServerSocket(socketPort);
        } catch (IOException e) {
            Log.E("Unable to start server " + e.getMessage());
            return;
        }
        serverRunning = true;
        ExecutorService executor = Executors.newFixedThreadPool(Configuration.getInstance().getThreadPoolSize());
        Log.I("Starting Server at " + socketPort);
        serverThread = (new Thread(() -> {
            while (serverRunning) {
                try {
                    Socket client = server.accept();
                    Log.I("Connection established with " + client.getInetAddress().getHostAddress());
                    executor.execute(() -> {
                        processClient(client);
                    });
                } catch (IOException e) {

                    Log.I(e.getMessage());
                }
            }
            try {
                server.close();
            } catch (IOException e) {
                Log.E("Error tying to stop server "+e.getMessage());
            }
        }));
        serverThread.setUncaughtExceptionHandler((t,err)->{
            Log.E("Error at server thread: "+err.getMessage()+" restarting server");
            try {
                if(serverThread.isAlive())
                    serverThread.interrupt();
                server.close();
                startServer();
            } catch (IOException e) {
                Log.E("Error trying to reset the socket: Failed shutting down "+ e.getMessage());
            }
        });
        serverThread.start();
    }

    public void stopServer(){
        serverRunning=false;
        if(serverThread!=null)
            serverThread.interrupt();
        try {
            if(server!=null)
                server.close();
        } catch (IOException e) {
            Log.E("Error tying to stop server "+e.getMessage());
        }
    }

}
