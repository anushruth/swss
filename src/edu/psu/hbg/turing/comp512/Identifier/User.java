package edu.psu.hbg.turing.comp512.Identifier;

import edu.psu.hbg.turing.comp512.Platform.PlatformBase;
import edu.psu.hbg.turing.comp512.Platform.PlatformError;
import org.json.JSONObject;

import java.security.cert.Certificate;

/**
 * Created by avs15 on 3/2/17.
 */
public class User implements Identity{
    private byte[] userID = null;
    private String name = null;
    private String group = null;
    private String digestAlgo = null;
    private String defaultSignatureAlgo = null;
    private Certificate userCertificate = null;
    private JSONObject marshalledUser = null;

    public User(String name, String group, byte[] userID ,String digestAlgo, String defaultSignature , Certificate certificate){
        this.userID = userID;
        this.name = name;
        this.userCertificate = certificate;
        this.group = group;
        this.digestAlgo = digestAlgo;
        this.defaultSignatureAlgo = defaultSignature;
    }

    public User(JSONObject usr) throws PlatformError {
        this.userID = PlatformBase.getInstance().stringToByte(usr.getString("ID"));
        this.name = usr.getString("Name");
        this.group = usr.getString("Group");
        this.digestAlgo = usr.getString("Digest Algo");
        this.defaultSignatureAlgo = usr.getString("Signature Algo");
        this.userCertificate = PlatformBase.getInstance().getCertificateFromCertificateString(usr.getString("Certificate"));
    }


    @Override
    public String getIDString() {
        return PlatformBase.getInstance().byteToString(userID);
    }

    @Override
    public byte[] getID() {
        return userID;
    }


    public void setUserID(byte[] userID) {
        this.userID = userID;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getGroup() {
        return group;
    }

    @Override
    public Certificate getCertificate() {
        return userCertificate;
    }

    @Override
    public String getCertificateString(){
        return PlatformBase.getInstance().getCertificateStringFromCertificate(userCertificate);
    }

    @Override
    public JSONObject getMarshalled(){
        if(marshalledUser == null){
            marshalledUser = new JSONObject();
            marshalledUser.put("ID", getIDString());
            marshalledUser.put("Name", getName());
            marshalledUser.put("Group", getGroup());
            marshalledUser.put("Digest Algo", getDigestAlgo());
            marshalledUser.put("Signature Algo", getSignatureAlgo());
            marshalledUser.put("Certificate", getCertificateString());
        }
        return marshalledUser;
    }
    @Override
    public String getDigestAlgo() {
        return digestAlgo;
    }
    @Override
    public String getSignatureAlgo() {
        return defaultSignatureAlgo;
    }
}
