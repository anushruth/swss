package edu.psu.hbg.turing.comp512.Identifier;

import edu.psu.hbg.turing.comp512.Communication.CommunicationError;
import edu.psu.hbg.turing.comp512.Config.ConfigurationError;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Platform.PlatformBase;
import edu.psu.hbg.turing.comp512.Platform.PlatformError;
import org.json.JSONObject;

import javax.xml.bind.DatatypeConverter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.cert.Certificate;

/**
 * @author Anushruth
 * @version 0.1
 */
public class Node implements Identity{
    private byte[] nodeID;
    private InetAddress nodeAddress;
    private int port;
    private String name;
    private String algorithm;
    private int keyLength;
    private String group;
    private String signatureAlgorithm;
    private String digestAlgorithm;
    private Certificate certificate;

    public static Node newNode(String name, String group, String algorithm, int keyLength, String signatureAlgorithm, InetAddress inetAddress, String digestAlgorithm, byte[] nodeID, Certificate certificate) throws CommunicationError, ConfigurationError, PlatformError {
        return new Node(name, group, algorithm, keyLength, signatureAlgorithm, inetAddress, digestAlgorithm, nodeID, certificate);
    }

    public static Node newNode(String name, String ID, InetAddress inetAddress, int port){
        if(name!=null && ID != null && inetAddress != null)
            return new Node(name, ID, inetAddress, port);
        else return null;
    }

    private Node(String name, String ID, InetAddress inetAddress, int port){
        this.nodeID = PlatformBase.getInstance().stringToByte(ID);
        this.name = name;
        this.port = port;
        this.nodeAddress = inetAddress;
    }

    private Node(String name, String group, String algorithm, int keyLength, String signatureAlgorithm, InetAddress inetAddress, String digestAlgorithm, byte[] nodeID, Certificate certificate) throws PlatformError, ConfigurationError, CommunicationError {
        this.name = name;
        this.nodeID = nodeID;
        this.nodeAddress = inetAddress;
        this.certificate = certificate;
        this.algorithm = algorithm;
        this.group = group;
        this.keyLength = keyLength;
        this.digestAlgorithm = digestAlgorithm;
        this.signatureAlgorithm = signatureAlgorithm;
        if(!PlatformBase.getInstance().verifyCertificate(nodeID,certificate,digestAlgorithm))
            throw new CommunicationError("Certificate cannot be verified");
    }

    public JSONObject getMarshalled(){
        JSONObject node = new JSONObject();
        node.put("Name", name);
        node.put("ID", DatatypeConverter.printHexBinary(nodeID));
        node.put("IP", nodeAddress.getHostAddress());
        node.put("Port", port);
//        node.put("Certificate", PlatformBase.getInstance().getCertificateStringFromCertificate(certificate));
//        node.put("Digest", digestAlgorithm);
        return node;
    }

    @Override
    public String getSignatureAlgo() {
        return null;
    }

    public Node(JSONObject node){
        name = node.getString("Name");
        if(node.has("ID")) nodeID = PlatformBase.getInstance().stringToByte(node.getString("ID"));
        try {
            nodeAddress = InetAddress.getByName(node.getString("IP"));
            port = node.getInt("port");
        } catch (UnknownHostException e) {
            Log.W("Couldn't parse node "+ name);
        }
    }

    @Override
    public byte[] getID() {
        return nodeID;
    }

    @Override
    public String getIDString(){
        return PlatformBase.getInstance().byteToString(nodeID);
    }

    public InetAddress getNodeAddress() {
        return nodeAddress;
    }

    public int getPort() {
        return port;
    }


    public String getName() {
        return name;
    }

    @Override
    public String getDigestAlgo() {
        return digestAlgorithm;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    @Override
    public String getCertificateString() {
        return null;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public int getKeyLength() {
        return keyLength;
    }

    public String getGroup() {
        return group;
    }

    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public boolean equals(Object o){
        if(o instanceof Node){
            return ((Node)o).getIDString().equalsIgnoreCase(getIDString());
        }
        return false;
    }

}
