package edu.psu.hbg.turing.comp512.Identifier;

import org.json.JSONObject;

import java.security.cert.Certificate;

/**
 * @author Anushruth
 * @version 0.1
 */
public interface Identity {
    String getIDString();
    byte[] getID();
    String getName();
    String getGroup();
    Certificate getCertificate();
    String getCertificateString();
    JSONObject getMarshalled();
    String getDigestAlgo();
    String getSignatureAlgo();
}
