package edu.psu.hbg.turing.comp512.DataBase;

import edu.psu.hbg.turing.comp512.Config.ConfigurationError;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Identifier.User;
import edu.psu.hbg.turing.comp512.Platform.PlatformBase;
import edu.psu.hbg.turing.comp512.Platform.PlatformError;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anushruth on 3/9/17.
 * <p>
 * <p> Class that encapsulates all the post functionality. Post is an entry in the microblogging network</p>
 */
public class Post {
    private String postID = null;
    private User origin;
    private byte[] nonce = null;
    private String post = null;
    private String prevPostID = null;
    private String signature = null;
    private Timestamp createdOn = null;
    private JSONObject marshaledPost = null;

    /**
     *      ==============================================================================
     *                          Constructors for the Post Object
     *      ==============================================================================
     */

    /**
     * Parses a JSONArray into a list of posts and returns the same
     *
     * @param postObjects JSONArray containing the list of posts
     * @return null on failure or if input is null, List of posts otherwise
     */
    public static List<Post> getPosts(JSONArray postObjects) {
        if ((postObjects != null) && (postObjects.length() > 0)) {
            List<Post> posts = new ArrayList<Post>(postObjects.length());
            for (int i = 0; i < postObjects.length(); i++) {
                JSONObject post = (JSONObject) postObjects.get(i);
                Post pst = getPost(post);
                if (pst != null)
                    posts.add(pst);
            }
            return posts;
        }
        Log.W("Received empty array??");
        return null;
    }

    /**
     * Parses a JSONObject into post
     *
     * @param post JSONObject containing the post
     * @return null if input is empty or if post couldn't be verified or malformed post. Post object otherwise
     */
    public static Post getPost(JSONObject post) {
        if (post != null) {
            Post newPost = new Post();
            newPost.marshaledPost = post;
            Log.I(post.get("Post ID").toString());
            newPost.postID = post.getString("Post ID");
            newPost.prevPostID = post.getString("Prev Post");
            newPost.nonce = post.has("Nonce") ? PlatformBase.getInstance().stringToByte((String) post.get("Nonce")) : null;
            newPost.post = post.getString("Post");
            newPost.origin = DataBaseManager.getInstance().getUser(post.getString("User"));
            newPost.createdOn = Timestamp.valueOf(post.getString("CreatedON"));
            newPost.signature = post.getString("Signature");
            if (newPost.postID == null || newPost.post == null || newPost.origin == null) {
                Log.E("Failed to convert post one or more fields is missing");
                return null;
            }
            if (newPost.origin == null) {
                Log.W("Cannot find user for post: " + newPost.postID);
                return null;
            }
//            try {
//                PlatformBase.getInstance().verifyPost(newPost);
//            } catch (PlatformError platformError) {
//                Log.W("Post verification failed for: " + newPost.postID + " [" + platformError.getMessage() + "]");
//                newPost = null;
//            } catch (ConfigurationError configurationError) {
//                Log.W("Post verification failed for: " + newPost.postID + " [" + configurationError.getMessage() + "]");
//                newPost = null;
//            }
            return newPost;
        }
        Log.E("Input empty object ??");
        return null;
    }

    /**
     * Creates a new post and returns back the post object
     *
     * @param origin   The user info of the user creating the post
     * @param nonce    Nonce for limiting the rate of posting. Not implemented yet
     * @param post     Post of the user
     * @param prevPost Link to the previous post of the user
     * @param password Character array containing the password of the user. Will be blanked after use
     * @return Post object on successfull creation of post. Null otherwise
     * @throws PlatformError      Thrown if there is a platform error when signing the post
     * @throws ConfigurationError Thrown if there is a configuration error when signing the post
     */
    public static Post newPost(User origin, byte[] nonce, String post, String prevPost, char[] password) throws PlatformError, ConfigurationError {
        if (origin == null || post == null || prevPost == null || password == null) {
            Log.E("One or more fields missing");
            return null;
        }
        Post newPost = new Post();
        newPost.origin = origin;
        newPost.nonce = nonce;
        newPost.post = post;
        newPost.prevPostID = prevPost;
        newPost.createdOn = new Timestamp(System.currentTimeMillis());
        newPost.marshaledPost = new JSONObject();
        newPost.marshaledPost.put("User", origin.getIDString());
        newPost.marshaledPost.put("CreatedON", newPost.createdOn.toString());
        newPost.marshaledPost.put("Post", post);
        newPost.marshaledPost.put("Prev Post", prevPost);
        newPost.marshaledPost.put("Nonce", PlatformBase.getInstance().byteToString(nonce));
        PlatformBase.getInstance().signPost(newPost, password);
        newPost.postID = (String) newPost.marshaledPost.get("Post ID");
        newPost.signature = (String) newPost.marshaledPost.get("Signature");
        return newPost;
    }

    /**
     * Static method to create new posts from available data. Mainly used by Database to create new post objects from its entries
     *
     * @param origin    Creator of the post
     * @param createdOn Timestamp of creation
     * @param post      Contents of the post
     * @param postID    Identifier of the post
     * @param prevPost  Identifier of the previous post
     * @param signature Signature of the post
     * @param nonce     Nonce appended to the post
     * @return
     */
    public static Post newPost(User origin, Timestamp createdOn, String post, String postID, String prevPost, String signature, byte[] nonce) {
        if (origin == null || post == null || prevPost == null || createdOn == null || postID == null || signature == null) {
            Log.E("Unable to create post due to one or more missing data");
            return null;
        }
        Post newPost = new Post();
        newPost.origin = origin;
        newPost.post = post;
        newPost.prevPostID = prevPost;
        newPost.signature = signature;
        newPost.nonce = nonce;
        newPost.postID = postID;
        newPost.createdOn = createdOn;
        newPost.marshaledPost = new JSONObject();
        newPost.marshaledPost.put("User", origin.getIDString());
        newPost.marshaledPost.put("CreatedON", (new Timestamp(System.currentTimeMillis())).toString());
        newPost.marshaledPost.put("Post", post);
        newPost.marshaledPost.put("Prev Post", prevPost);
        newPost.marshaledPost.put("Post ID", postID);
        if (nonce != null) newPost.marshaledPost.put("Nonce", PlatformBase.getInstance().byteToString(nonce));
        newPost.marshaledPost.put("Signature", signature);
        return newPost;
    }

    /**
     *      ==============================================================================
     *                          End Constructors for the Post Object
     *      ==============================================================================
     */


    /**
     *      ==============================================================================
     *                          Getters for fields in the Post Object
     *      ==============================================================================
     */

    /**
     * Getter for JSON formatted post
     *
     * @return Post in JSON format
     */
    public JSONObject getMarshaledPost() {
        return marshaledPost;
    }

    /**
     * Getter for Timestamp of creation
     *
     * @return Timestamp representing the date of creation
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Getter for post ID
     *
     * @return String representing the post ID
     */
    public String getPostID() {
        return postID;
    }

    /**
     * Getter for previous post link
     *
     * @return String representing the previous post ID
     */
    public String getPrevPostID() {
        return prevPostID;
    }

    /**
     * Getter for user object representing creator
     *
     * @return User object representing the creator
     */
    public User getOrigin() {
        return origin;
    }

    /**
     * Getter for byte array containing nonce. Not used presently
     *
     * @return byte array containing nonce
     */
    public byte[] getNonce() {
        return nonce;
    }

    /**
     * Getter for string containing post
     *
     * @return String containing post
     */
    public String getPost() {
        return post;
    }

    /**
     * Getter for string containing the signature for the post
     *
     * @return String containing signature
     */
    public String getSignature() {
        return signature;
    }

    /*
     *      ==============================================================================
     *                        End Getters for fields in the Post Object
     *      ==============================================================================
     */

    /*
     *      ==============================================================================
     *                        Log function for the post
     *      ==============================================================================
     */

    public void logPost() {
        Log.I("===============================================================================================");
        Log.I("ID:            " + postID);
        Log.I("Origin:        " + origin.getName() + " : " + origin.getIDString());
        Log.I("Created on:    " + createdOn.toString());
        Log.I("Post:          " + post);
        Log.I("PrevPost:      " + prevPostID);
        Log.I("===============================================================================================");
    }
}
