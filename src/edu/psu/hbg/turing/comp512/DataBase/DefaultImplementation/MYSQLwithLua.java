package edu.psu.hbg.turing.comp512.DataBase.DefaultImplementation;

import edu.psu.hbg.turing.comp512.DataBase.DataBaseManager;
import edu.psu.hbg.turing.comp512.DataBase.Post;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Identifier.Identity;
import edu.psu.hbg.turing.comp512.Identifier.User;
import edu.psu.hbg.turing.comp512.Platform.PlatformBase;
import edu.psu.hbg.turing.comp512.Platform.PlatformError;
import edu.psu.hbg.turing.comp512.Utils.XORComparator;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.ResourceFinder;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.JsePlatform;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

/**
 * @author Anushruth
 * @version 0.1
 *          <p>Experimental implementation of Database handler. Honestly this is not probably correct. Need someone with
 *          good knowledge of database to look into it. I still need to take database class [lol]</p>
 */
public class MYSQLwithLua implements DataBaseManager.DataBase, ResourceFinder {

    private Globals globals;
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost/kadmelia?autoReconnect=true&useSSL=false";
    private Connection conn = null;
    private HashMap<String, User> userHashMap = null;

    /**
     * Crude implementation of a cache. Basically store 10000 entries in to the hash map.
     * If more come in then kick one out at random. If proper implementation is required
     * This has to change based on some some heuristic. Probably last seen or priority queue
     * like the one in XORRouting with prefrence to nodes nearer.
     * TODO: Change the implementation to a distance based cache rather than randomly selected one in future
     *
     * @param user Object to be updated
     */
    private void updateHashMap(User user) {
        if (user != null) {
            synchronized (userHashMap) {
                if (!userHashMap.containsKey(user.getIDString())) {
                    userHashMap.put(user.getIDString(), user);
                }
                if (userHashMap.size() > 10000) {
                    String[] keys = (String[]) userHashMap.keySet().toArray();
                    userHashMap.remove(keys[(new Random()).nextInt(keys.length - 1)]);
                }
            }
        }
    }

    /**
     * Refer to the comment for update hash map
     *
     * @param user
     * @return
     */
    private User getUserFomHashMap(String user) {
        if (user != null) {
            synchronized (userHashMap) {
                return userHashMap.get(user);
            }
        }
        return null;
    }

    /**
     * Initialize the database with credentials and filter
     *
     * @param user     User for the database
     * @param password Password for the database
     * @param filter   Filter to be used while updating the database
     * @return true is initialization was successful, false otherwise
     */
    @Override
    public boolean init(String user, String password, String filter) {
        userHashMap = new HashMap<>();
        LuaValue ftr = CoerceJavaToLua.coerce(this);
        globals = JsePlatform.standardGlobals();
        this.globals.finder = this;
        globals.loadfile(filter).call(ftr);
        try {
            Class.forName(JDBC_DRIVER);
            Log.I("Connecting to database");
            conn = DriverManager.getConnection(DB_URL, user, password);
        } catch (ClassNotFoundException e) {
            Log.I(e.getMessage());
            return false;
        } catch (SQLException e) {
            Log.I(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Queries the lua filter to check if this post is required to be updated
     * if so writes to the database. Else throws away the post
     *
     * @param post Post to be updated
     * @return true if the update actually happened. False otherwise
     */
    @Override
    public boolean savePost(Post post) {
        if (post != null) {
            LuaValue filter = globals.get("post_save_filter");
            if (!filter.isnil()) {
                LuaValue save = filter.call(CoerceJavaToLua.coerce(post));
                if (!save.isboolean()) {
                    Log.E("Buggy filter implementation. Check filter");
                    return false;
                } else {
                    if (save.checkboolean()) {
                        PreparedStatement stmt = null;
                        try {
                            String prevPost = post.getPrevPostID();
                            if (prevPost == null)
                                return false;
                            /**
                             * If the prevPost says First Post and there are no posts for this user presently and if there isn't already a copy then write to DB
                             */
                            if (((prevPost.equalsIgnoreCase("First Post") && getRecentPostsFromUser(post.getOrigin(), 1).size() == 0))
                                    || (getPost(prevPost) != null)) {
                                synchronized (conn) {
                                    stmt = conn.prepareStatement("Insert into post (createdBy, id, createdON, receivedON, post, prevPost, nonce, signature) VALUES (\""
                                            + post.getOrigin().getIDString() + "\",\"" + post.getPostID() + "\",?,?,?,\"" + post.getPrevPostID() + "\", \"" +
                                            PlatformBase.getInstance().byteToString(post.getNonce()) + "\",\"" + post.getSignature() + "\");");
                                    stmt.setTimestamp(1, post.getCreatedOn());
                                    stmt.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                                    stmt.setString(3, post.getPost());
                                    stmt.execute();
                                }
                            } else {
                                Log.I("Cannot accept post wrong linkage");
                                return false;
                            }
                        } catch (SQLException e) {
                            Log.I(e.getMessage());
                            return false;
                        }
                    }
                }
            } else {
                Log.I("Filter not found. Check the lua script");
                return false;
            }
            return true;
        }
        Log.E("Empty post ??");
        return false;
    }

    /**
     * Get the user associated with the given ID
     *
     * @param userID String containing the user ID
     * @return User object if it exists, null otherwise
     */
    @Override
    public User getUser(String userID) {
        if (userID == null) {
            Log.W("UserID null?");
            return null;
        }
        User user = getUserFomHashMap(userID);
        if (user != null)
            return user;
        try {
            ResultSet rs = null;
            synchronized (conn) {
                PreparedStatement stmt = conn.prepareStatement("SELECT id, name, groupName, digest, signature, certificate FROM user WHERE id = \"" + userID + "\"");
                if (stmt.execute())
                    rs = stmt.getResultSet();
            }
            if (rs != null && rs.next()) {
                User usr = new User(rs.getString("name"), rs.getString("groupName"), PlatformBase.getInstance().stringToByte(rs.getString("id")),
                        rs.getString("digest"), rs.getString("signature"),
                        PlatformBase.getInstance().getCertificateFromCertificateString(rs.getString("certificate")));

                if (usr != null) {
                    Log.I("Found User");
                    updateHashMap(user);
                }
                return usr;
            }
        } catch (SQLException e) {
            Log.W("Error finding the user: " + userID + " " + e.getMessage());
        } catch (PlatformError platformError) {
            Log.W("Error parsing the user: " + userID + " " + platformError.getMessage());
        }
        return null;
    }

    /**
     * Save the user to database if the entry doesn't already exist and the filter agrees
     *
     * @param user User to be saved
     */
    @Override
    public boolean saveUser(User user) {
        if (user == null) {
            Log.I("User is null?");
            return false;
        }
        LuaValue filter = globals.get("user_save_filter");
        if (!filter.isnil()) {
            LuaValue save = filter.call(CoerceJavaToLua.coerce(user));
            if (!save.isboolean()) {
                Log.E("Buggy filter implementation. Check filter");
                return false;
            } else {
                if (save.checkboolean()) {
                    PreparedStatement stmt;
                    try {
                        synchronized (conn) {
                            stmt = conn.prepareStatement("Insert into user (id, name, groupName, digest, signature, certificate) VALUES (\"" + user.getIDString()
                                    + "\",\"" + user.getName() + "\",\"" + user.getGroup() + "\",\"" + user.getDigestAlgo() + "\", \"" + user.getSignatureAlgo() +
                                    "\",\"" + user.getCertificateString() + "\");");
                            executeStatement(stmt);
                        }
                        Log.I("Saved " + user.getName() + " : " + user.getIDString() + " to DB");
                        return true;
                    } catch (SQLException e) {
                        Log.W("SQL Error: " + e.getMessage());
                        return false;
//                    } catch (ConfigurationError configurationError) {
//                        Log.W("Cannot add user: " + user.getName() + " : " + user.getIDString() + " " + configurationError.getMessage());
//                        return false;
//                    } catch (PlatformError platformError) {
//                        Log.W("Cannot add user: " + user.getName() + " : " + user.getIDString() + " " + platformError.getMessage());
//                        return false;
                    }
                }
            }
        } else {
            Log.E("No filter found");
        }
        return false;
    }


    /**
     * Get post specified by the given post ID
     *
     * @param postID String containing the post ID
     * @return
     */
    @Override
    public Post getPost(String postID) {
        try {
            ResultSet rs = null;
            synchronized (conn) {
                PreparedStatement stmt = conn.prepareStatement("SELECT createdBy, id, createdON, receivedON, post, prevPost," +
                        " nonce, signature FROM post WHERE id = \"" + postID + "\"");
                rs = executeStatement(stmt);
            }
            Post post = null;
            if (rs != null && rs.next()) {
                DataBaseManager dbm = DataBaseManager.getInstance();
                PlatformBase platform = PlatformBase.getInstance();
                post = Post.newPost(dbm.getUser(rs.getString("createdBY")), rs.getTimestamp("createdON"), rs.getString("post"),
                        rs.getString("id"), rs.getString("prevPost"), rs.getString("signature"),
                        PlatformBase.getInstance().stringToByte(rs.getString("nonce")));
            }
            return post;
        } catch (SQLException e) {
            Log.W("Unable to find the post " + postID);
        }
        return null;
    }

    /**
     * Get recent posts up to a number limit
     *
     * @param limit Limit to number of posts to retrieve
     * @return List of posts
     */
    @Override
    public List<Post> getRecentPosts(int limit) {
        try {
            ResultSet rs = null;
            synchronized (conn) {
                PreparedStatement stmt = conn.prepareStatement("SELECT createdBy, id, createdON, receivedON, post, prevPost, nonce, signature" +
                        " FROM post ORDER BY createdON LIMIT " + limit);
                rs = executeStatement(stmt);
            }
            List<Post> posts = new ArrayList<>();
            while (rs != null && rs.next() && limit > 0) {
                DataBaseManager dbm = DataBaseManager.getInstance();
                PlatformBase platform = PlatformBase.getInstance();
                Log.I("Found " + rs.getString("post"));
                Post pst = Post.newPost(dbm.getUser(rs.getString("createdBY")), rs.getTimestamp("createdON"), rs.getString("post"),
                        rs.getString("id"), rs.getString("prevPost"), rs.getString("signature"), platform.stringToByte(rs.getString("nonce")));
                if (pst != null) {
                    posts.add(pst);
                    limit--;
                }//Remove post if corrupted
            }
            return posts;
        } catch (SQLException e) {
            Log.W("Unable to find posts " + e.getMessage());
            return null;
        }
    }

    /**
     * Get all the posts that are generated after a time stamp.
     * TODO: Check if this interface is really required. Might lead to Out of memory error???
     *
     * @param limit Timestamp of the post to be limited
     * @return List of posts
     */
    @Override
    public List<Post> getRecentPosts(Timestamp limit) {
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT createdBy, id, createdON, receivedON, post, prevPost," +
                    " nonce, signature FROM post ORDER BY createdON");
            ResultSet rs = executeStatement(stmt);
            List<Post> posts = new ArrayList<>();
            while (rs.next()) {
                DataBaseManager dbm = DataBaseManager.getInstance();
                PlatformBase platform = PlatformBase.getInstance();
                Log.I("Found " + rs.getString("post"));
                Post pst = Post.newPost(dbm.getUser(rs.getString("createdBY")), rs.getTimestamp("createdON"),
                        rs.getString("post"), rs.getString("id"), rs.getString("prevPost"),
                        rs.getString("signature"), platform.stringToByte(rs.getString("nonce")));
                if (pst != null) {
                    if (limit.before(pst.getCreatedOn()))
                        break;
                    posts.add(pst);
                }//TODO: Remove post if corrupted
            }
            return posts;
        } catch (SQLException e) {
            Log.W("Unable to find posts " + e.getMessage());
            return null;
        }
    }

    /**
     * Get first 'limit' number of posts from the given user
     *
     * @param user  Target user
     * @param limit Maximum number of posts to retrieve
     * @return List containing the posts requested null if no entries were found or on database error
     */
    @Override
    public synchronized List<Post> getRecentPostsFromUser(User user, int limit) {
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT createdBy, id, createdON, receivedON, post, prevPost, " +
                    "nonce, signature FROM post WHERE createdBy = \"" + user.getIDString() + "\" ORDER BY createdON DESC");
            ResultSet rs = executeStatement(stmt);
            List<Post> posts = new ArrayList<>();
            while (rs.next() && limit > 0) {
                DataBaseManager dbm = DataBaseManager.getInstance();
                PlatformBase platform = PlatformBase.getInstance();
                Log.I("Found " + rs.getString("post"));
                String userName = rs.getString("createdBY");
                String post = rs.getString("post");
                Timestamp creation = rs.getTimestamp("createdON");
                String id = rs.getString("id");
                String prevPost = rs.getString("prevPost");
                String signature = rs.getString("signature");
                String nonce = rs.getString("nonce");
                if (userName != null && post != null && id != null && prevPost != null && signature != null) {
                    Post pst = Post.newPost(dbm.getUser(userName), creation, post, id, prevPost, signature,
                            ((nonce != null && !nonce.equalsIgnoreCase("null")) ? platform.stringToByte(nonce) : null));
                    if (pst != null) {
                        posts.add(pst);
                        limit--;
                    }//Remove post if corrupted
                }
            }
            return posts;
        } catch (SQLException e) {
            Log.W("Unable to find posts for " + user.getName() + " : " + user.getIDString());
            return null;
        }
    }

    /**
     * Get posts from the user up to the limit given by the timestamp
     *
     * @param user  User object
     * @param limit Limit as timestamp object
     * @return List containing posts of the the user upto the given limit null on error
     */
    @Override
    public synchronized List<Post> getRecentPostsFromUser(User user, Timestamp limit) {
        Log.I("Attempting to get posts");
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT createdBy, id, createdON, receivedON, post, prevPost, nonce, signature" +
                    " FROM post WHERE createdBy = \"" + user.getIDString() + "\" ORDER BY createdON DESC");
            ResultSet rs = executeStatement(stmt);
            ArrayList<Post> posts = new ArrayList<>();
            while (rs.next()) {
                DataBaseManager dbm = DataBaseManager.getInstance();
                PlatformBase platform = PlatformBase.getInstance();
                Post post = Post.newPost(dbm.getUser(rs.getString("createdBY")), rs.getTimestamp("createdON"), rs.getString("post"),
                        rs.getString("id"), rs.getString("prevPost"), rs.getString("signature"),
                        PlatformBase.getInstance().stringToByte(rs.getString("nonce")));
                if (post.getCreatedOn().before(limit))
                    break;
                posts.add(post);
            }
            Log.I("Retrieved " + posts.size() + " from " + user.getName());
            return posts;
        } catch (SQLException e) {
            Log.W("Unable to find posts for " + user.getName() + " : " + user.getIDString());
        }
        return null;
    }

    @Override
    public List<User> getUsersWithAffinityTo(Identity target, int limit) {
        List<User> userList = new ArrayList<>();
        synchronized (userHashMap){
            PriorityQueue<User> userPriorityQueue = new PriorityQueue<>(new XORComparator(target));
            while(!userPriorityQueue.isEmpty() && limit>0 ){
                userList.add(userPriorityQueue.remove());
                limit--;
            }
        }
        if(userList.size()>0)
            return userList;
        return null;
    }

    @Override
    public InputStream findResource(String s) {
        try {
            return new FileInputStream(new File(s));
        } catch (FileNotFoundException e) {
            Log.I(e.getMessage());
        }
        return null;
    }

    /**
     * Method to execute the database query statements. As concurrency is not supported by the driver this is used
     *
     * @param stmt Statement to be executed
     * @return Result of the execution null is no result is available
     */
    private synchronized ResultSet executeStatement(PreparedStatement stmt) {
        if (stmt != null) {
            try {
                if (stmt.execute())
                    return stmt.getResultSet();
            } catch (SQLException e) {
                Log.W("Couldn't execute database query " + e.getMessage());
            }
        }
        return null;
    }
}
