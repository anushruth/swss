package edu.psu.hbg.turing.comp512.DataBase.DefaultImplementation;

import edu.psu.hbg.turing.comp512.Config.Configuration;
import edu.psu.hbg.turing.comp512.DataBase.DataBaseManager;
import edu.psu.hbg.turing.comp512.DataBase.Post;
import edu.psu.hbg.turing.comp512.Identifier.Identity;
import edu.psu.hbg.turing.comp512.Identifier.User;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Utils.XORComparator;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created for testing purposes. Never meant to be used in an actual scenario. Basically helps out in situations where
 * testing with out installing a database server is desirable
 */
public class InMemHashMap implements DataBaseManager.DataBase{

    private HashMap<String, User> userMap;
    private HashMap<String, Post> postMap;

    private int userLimit = 10;
    private int postsLimit = 100;

    public InMemHashMap(){
        userMap = new HashMap<>();
        postMap = new HashMap<>();
    }

    @Override
    public boolean init(String user, String password, String filter) {
        return false;
    }

    /**
     * Saves the post of only
     * @param post  Post to be saved to the database
     * @return
     */
    @Override
    public boolean savePost(Post post) {
        synchronized (postMap) {
            if (postMap.size() < postsLimit) {
                if (saveUser(post.getOrigin())) {
                    postMap.put(post.getPostID(), post);
                    return true;
                }
                return false;
            } else {
                if (saveUser(post.getOrigin())) {
                    List<Post> posts = new ArrayList(postMap.values());
                    Collections.sort(posts, Comparator.comparing(Post::getCreatedOn));
                    Log.I("DB full Removing:"  + posts.get(0).getPost()+" From "+ posts.get(0).getOrigin().getName());
                    postMap.remove(posts.get(0).getPostID());
                    postMap.put(post.getPostID(), post);
                    return true;
                }
                return false;
            }
        }
    }

    @Override
    public User getUser(String userID) {
        synchronized (userMap) {
            return userMap.get(userID);
        }
    }

    @Override
    public boolean saveUser(User user) {
        synchronized (userMap) {
            if (userMap.values().stream().anyMatch(user1 -> user1.getIDString().equalsIgnoreCase(user.getIDString())))
                return true;
            if (userMap.size() > userLimit) { //If over the limit. Remove the one farthest from the user
                PriorityQueue<User> userPriorityQueue = new PriorityQueue<>(new XORComparator(Configuration.getInstance().getNodeObject()).reverse());
                userPriorityQueue.addAll(userMap.values());
                User removeCandidate = userPriorityQueue.remove();
                if (XORComparator.getDistanceBetween(user.getID(), removeCandidate.getID()) < 0) {
                    userMap.remove(removeCandidate.getIDString());
                    removePostsFromUser(removeCandidate);
                }
                else
                    return false;
            }
            userMap.put(user.getIDString(), user);
            return true;
        }
    }

    @Override
    public Post getPost(String postID) {
        synchronized (postMap) {
            return postMap.get(postID);
        }
    }

    @Override
    public List<Post> getRecentPosts(int limit) {
        synchronized (postMap) {
            Collection<Post> postList = postMap.values();
            List<Post> posts = new ArrayList(postList);
            Collections.sort(posts, Comparator.comparing(Post::getCreatedOn));
            if (posts.size() > limit)
                return posts.subList(0, limit);
            else
                return posts;
        }
    }

    @Override
    public List<Post> getRecentPosts(Timestamp limit) {
        synchronized (postMap) {
            return postMap.values().stream().filter(post -> (post.getCreatedOn().after(limit))).collect(Collectors.toList());
        }
    }

    @Override
    public List<Post> getRecentPostsFromUser(User user, int limit) {
        synchronized (postMap) {
            List<Post> posts = postMap.values().stream().filter(post -> (post.getOrigin().getIDString().compareTo(user.getIDString()) == 0)).collect(Collectors.toList());
            Collections.sort(posts, Comparator.comparing(Post::getCreatedOn));
            if (posts.size() > limit)
                return posts.subList(posts.size()-limit, posts.size());
            else
                return posts;
        }
    }

    private void removePostsFromUser(User user){
        synchronized (postMap){
            postMap.values().removeIf(post->post.getOrigin().getIDString().equalsIgnoreCase(user.getIDString()));
            System.gc();
        }
    }

    @Override
    public List<Post> getRecentPostsFromUser(User user, Timestamp limit) {
        return null;
    }

    @Override
    public List<User> getUsersWithAffinityTo(Identity target, int limit) {
        synchronized (userMap) {
            PriorityQueue<User> users = new PriorityQueue<>(new XORComparator(target));
            users.addAll(userMap.values());
            List<User> userList = new ArrayList<>();
            while (!users.isEmpty() && limit > 0) {
                userList.add(users.remove());
                limit--;
            }
            return userList;
        }
    }
}
