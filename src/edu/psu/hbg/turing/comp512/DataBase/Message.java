package edu.psu.hbg.turing.comp512.DataBase;

import edu.psu.hbg.turing.comp512.Config.Configuration;
import edu.psu.hbg.turing.comp512.Identifier.Node;
import edu.psu.hbg.turing.comp512.Identifier.User;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Platform.PlatformError;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author anushruth
 * @since 3/2/17
 * <p>
 * <p>Message class encapsulates all the messaging API required to perform the post, fetch actions</p>
 * <p>
 * <p>Each Message has the following format</p>
 * <p>
 * <p>
 * <table> <tr> <th>Node</th>  <td>Contains ID of the source</td> </tr></table>
 * <table> <tr> <th>Port</th>  <td>Contains port number of the source node</td></tr></table>
 * <table> <tr> <th>Type</th>  <td>Number indicating the type of the message</td></tr></table>
 * <table> <tr> <th>Limit</th> <td>Incase of a request message maximum size of reply</td></tr></table>
 * <table> <tr> <th>Users</th> <td>Array containing user info</td></tr></table>
 * <table> <tr> <th>Posts</th> <td>Array containing the posts</td></tr></table>
 * <table> <tr> <th>User Info Req</th> <td>Array containing requests related to user</td></tr></table>
 * <table> <tr> <th>Post Req</th> <td>Array containing posts requested</td></tr></table>
 * <table> <tr> <th>Nodes</th> <td>Node map provided by the origin</td></tr></table>
 * <p>
 * <p>Depending on the type of the message the fields may or may not exist except for Node, Port and Type
 * All messages are encapsulated into JSON format. And any message not in this format will be rejected</p>
 */
public class Message {

    public enum MessageType {
        MESSAGE_UPDATE,
        MESSAGE_REQ_UPDATE,
        MESSAGE_REQ_NODES,
        MESSAGE_ACK,
        MESSAGE_NAK
    }

    private MessageType type = null;
    private JSONObject messageObject = null;
    private Node origin = null;
    private List<Post> posts = null;
    private List<User> users = null;
    private List<String> userInfoReq = null;
    private List<String> postReq = null;
    private List<String> nodeInfoReq = null;
    private List<Node> nodes = null;
    private String message = null;
    private Integer limit = null;
    private Timestamp upto = null;

    public Message(MessageType type, List<User> users, List<Post> posts, List<Node> nodes, List<String> userInfoReq, List<String> postReq, List<String> nodeInfoReq,Integer limit) {
        this.type = type;
        this.users = users;
        this.posts = posts;
        this.userInfoReq = userInfoReq;
        this.postReq = postReq;
        this.nodes = nodes;
        this.nodeInfoReq = nodeInfoReq;
        this.origin = Configuration.getInstance().getNodeObject();
        this.limit = limit;
    }

    /**
     * Build a message object from the received JSON message.
     *
     * @param msg    JSON formatted message
     * @param client Socket of the client that sent the message
     */
    public Message(JSONObject msg, Socket client) {
        if (msg != null && client != null) {
//            Log.I("Message: " + msg.toString());
            String originID = msg.getString("Node");
            String originNodeName = msg.getString("Node Name");
            int port = msg.getInt("Port");
            if (originID != null && originNodeName != null)
                origin = Node.newNode(originNodeName, originID, client.getInetAddress(), port);
            type = MessageType.values()[msg.getInt("Type")];
            if (msg.has("Limit"))
                limit = msg.getInt("Limit");
            if (msg.has("Users")) {
                Log.I("Parsing users sent by " + client.getInetAddress().getHostName());
                JSONArray jsonArray = (JSONArray) msg.get("Users");
                users = new ArrayList<>(jsonArray.length());
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject user = (JSONObject) jsonArray.get(i);
                        User usr = DataBaseManager.getInstance().getUser((String) user.get("ID"));
                        if (usr == null) {
                            usr = new User(user);
                            if (DataBaseManager.getInstance().saveUser(usr)) {
                                users.add(usr);
                            }
                        }
                    } catch (PlatformError platformError) {
                        Log.W("Unable to parse user certificate " + platformError.getMessage());
                    }
                }
            }
            if (msg.has("Posts")) {
                Log.I("Parsing posts sent by " + client.getInetAddress().getHostName());
                JSONArray jsonArray = (JSONArray) msg.get("Posts");
                if (jsonArray != null)
                    posts = Post.getPosts(jsonArray);
                if(posts!=null)
                    for (Post post : posts) {
                        post.logPost();
                        if (DataBaseManager.getInstance().getPost(post.getPostID()) == null)
                            DataBaseManager.getInstance().savePost(post);
                    }
            }
            if (msg.has("Nodes")) {
                Log.I("Parsing nodes sent by " + client.getInetAddress().getHostName());
                JSONArray jsonArray = (JSONArray) msg.get("Nodes");
                nodes = new ArrayList<>(jsonArray.length());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    String ip = obj.getString("IP");
                    String name = obj.getString("Name");
                    int prt = obj.getInt("Port");
                    String id = obj.getString("ID");
                    if (ip != null && id != null && name != null) {
                        try {
                            InetAddress address = InetAddress.getByName(ip);
                            nodes.add(Node.newNode(name, id, address, prt));
                        } catch (UnknownHostException e) {
                            Log.I("Nothing found at " + ip + " ignoring");
                        }
                    }
                }
            }
            if (msg.has("User Info Req")) {
                Log.I("Parsing user info req sent by " + client.getInetAddress().getHostName());
                JSONArray jsonArray = (JSONArray) msg.get("User Info Req");
                userInfoReq = new ArrayList<>(jsonArray.length());
                for (int i = 0; i < jsonArray.length(); i++) {
                    userInfoReq.add(jsonArray.getString(i));
                }
            }
            if (msg.has("Node Info Req")) {
                Log.I("Parsing node info req sent by " + client.getInetAddress().getHostName());
                JSONArray jsonArray = (JSONArray) msg.get("Node Info Req");
                nodeInfoReq = new ArrayList<>(jsonArray.length());
                for (int i = 0; i < jsonArray.length(); i++) {
                    nodeInfoReq.add(jsonArray.getString(i));
                }
            }
            if (msg.has("Post Req")) {
                Log.I("Parsing post req sent by " + client.getInetAddress().getHostName());
                JSONArray jsonArray = (JSONArray) msg.get("Post Req");
                postReq = new ArrayList<>(jsonArray.length());
                for (int i = 0; i < jsonArray.length(); i++) {
                    postReq.add(jsonArray.getString(i));
                }
            }
        } else Log.E("Cannot create message one or more of the inputs is null");
    }

    /**
     * Constructs the string containing the message to be transmitted. Used by the Comms to send messages
     */
    public void build() {
        JSONObject msg = new JSONObject();
        msg.put("Node", origin.getIDString());
        msg.put("Node Name", origin.getName());
        msg.put("Port", Configuration.getInstance().getServerPort()); //Change to configured port
        msg.put("Type", type.ordinal());
        if (limit != null)
            msg.put("Limit", limit);
        if (users != null) {
            JSONArray userList = new JSONArray();
            for (User usr : users) {
//                try {
                    userList.put(usr.getMarshalled());
//                } catch (PlatformError platformError) {
//                    Log.W("Unable to get user: " + usr.getName() + " " + platformError.getMessage());
//                } catch (ConfigurationError configurationError) {
//                    Log.W("Unable to get user: " + usr.getName() + " " + configurationError.getMessage());
//                }
            }
            msg.put("Users", userList);
        }
        if (posts != null) {
            JSONArray postList = new JSONArray();
            for (Post post : posts) {
                postList.put(post.getMarshaledPost());
            }
            msg.put("Posts", postList);
        }
        if (userInfoReq != null) {
            JSONArray users = new JSONArray();
            for (String user : userInfoReq) {
                users.put(user);
            }
            msg.put("User Info Req", users);
        }
        if (nodeInfoReq != null) {
            JSONArray nodes = new JSONArray();
            for (String node : nodeInfoReq) {
                nodes.put(node);
            }
            msg.put("Node Info Req", users);
        }
        if (postReq != null) {
            JSONArray req = new JSONArray();
            for (String postID : postReq) {
                req.put(postID);
            }
            msg.put("Post Req", req);
        }
        if (nodes != null) {
            JSONArray nodeList = new JSONArray();
            for (Node node : nodes) {
                nodeList.put(node.getMarshalled());
            }
            msg.put("Nodes", nodeList);
        }
        messageObject = msg;
        message = msg.toString();
        //Log.I("Message: "+ message);
    }

    public int getMessageSize() {
        return message.length();
    }

    public String getMessage() {
        return message;
    }

    public Message() {
    }

    public Node getOrigin() {
        return origin;
    }

    public String getMessageString() {
        return messageObject.toString();
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public List<User> getUsers() {
        return users;
    }

    public List<String> getUserInfoReq() {
        return userInfoReq;
    }

    public List<String> getNodeInfoReq() {
        return nodeInfoReq;
    }

    public List<String> getPostReq() {
        return postReq;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public Integer getLimit() {
        return limit;
    }
}
