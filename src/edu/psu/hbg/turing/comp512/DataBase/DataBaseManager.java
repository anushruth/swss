package edu.psu.hbg.turing.comp512.DataBase;

import edu.psu.hbg.turing.comp512.DataBase.DefaultImplementation.MYSQLwithLua;
import edu.psu.hbg.turing.comp512.Identifier.Identity;
import edu.psu.hbg.turing.comp512.Identifier.User;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author anushruth
 * @version 0.1
 *          <p>Database Manager is the interface that handles all the data. It is responsible for deleting the
 *          posts that are not required and updating the ones required and dropping he ones that aren't </p>
 */
public class DataBaseManager {

    /**
     * Instance variables for internal use
     */
    private static DataBaseManager dbm = null;
    private static DataBase db = null;

    /**
     * Interface for database implementation. It is not final and may need some changes. Inputs welcome
     */
    public interface DataBase {

        /**
         * <p>Init method that the can be used to initialize the implementation. An implementaion might decide to
         * not use it and handle it in the constructor. It is left to the implementation to decide</p>
         * @param user      user name for the database access account
         * @param password  password for data base access account
         * @param filter    filter to be used in the database. This allows for dynamic filtering of data
         * @return  true on success false otherwise
         */
        boolean init(String user, String password, String filter);

        /**
         * Save the post to the underlying database. However this doesn't guarantee that the post will be saved. It is up to
         * the implementation to decide what to do with the post. Here it will call the filter to decide
         * @param post  Post to be saved to the database
         * @return  true if the post was saved to the database false otherwise
         */
        boolean savePost(Post post);

        /**
         * Get the user object corresponding to the ID provided
         * @param userID    string containing the identifier for the user object
         * @return  user object representing the user identified by the ID if it exists. Null otherwise
         */
        User getUser(String userID);

        /**
         * Save the user to the database. This doesn't guarantee that the user user is saved to the underlying DB. Filter might be
         * consulted to take that decision
         * @param user  User object to be saved
         * @return  true if the data was saved successfully to the database. False otherwise
         */
        boolean saveUser(User user);

        /**
         * Get post represented by the ID given
         * @param postID    ID of the post
         * @return  Post object if a copy of the post exists, null otherwise
         */
        Post getPost(String postID);

        /**
         * Get the most recent posts available locally. Calculated by the timestamp
         * @param limit maximum number of posts to retrieve
         * @return  List of posts or null if DB is empty
         */
        List<Post> getRecentPosts(int limit);

        /**
         * Get the most recent posts ordered by the timestamp that claim to have been created after the given timestamp
         * Avoid using this method. Because it is possible that there might be a huge number of posts before the timestamp
         * @param limit
         * @return
         */
        List<Post> getRecentPosts(Timestamp limit);

        /**
         * Similar to {@link DataBase#getRecentPosts(int)} but only from the given user
         * @param user      User object for the posts requested
         * @param limit     maximum number of posts requested
         * @return  List of posts if available, null otherwise
         */
        List<Post> getRecentPostsFromUser(User user, int limit);

        /**
         * Similar to {@link DataBase#getRecentPosts(Timestamp)}. But only created by the user. It is important to note that
         * it might not be a good idea to use this
         * @param user      user object for the posts
         * @param limit     timestamp to limit the number of posts requested
         * @return  List containing the posts if available, null otherwise
         */
        List<Post> getRecentPostsFromUser(User user, Timestamp limit);

        /**
         * Get users with affinity to the the target Identifier. Doesn't search the whole DB. Just pulls from the cache
         * @param target  Object of type Identity
         * @param limit     Limit to number of users to get
         * @return          List of users if any found, null otherwise
         */
        List<User> getUsersWithAffinityTo(Identity target, int limit);
    }

    /**
     * Optinally initialize the database with another implementation
     *
     * @param db The object implementing the database interface
     */
    public static void init(DataBase db) {
        DataBaseManager.db = db;
    }

    /**
     * Initialization to be passed on to the database implementation. This is only here
     * because of the MySQL implementation. The in memory hashmap based non persistent database
     * doesn't require this
     *
     * @param user     User name to connect to the database server
     * @param password Password to connect to the database server
     * @param filter   Filter [Its here due to the Lua filter implementation]
     *                 <p>
     *                 TODO: Review this interface, Find a better way to do it
     */
    public static void initDataBase(String user, String password, String filter) {
        db = new MYSQLwithLua();
        db.init(user, password, filter);
    }

    /**
     * Getter for user. Queries the database and creates a new object for the user
     *
     * @param ID Identifier for the User
     * @return User object representing the user
     */
    public synchronized User getUser(String ID) {
        return db.getUser(ID);
    }

    /**
     * Add user to the map if it doesn't exist and then add the user to the DB
     *
     * @param user User object to be saved to the Database
     * @return True if the write is successful, false otherwise
     */
    public synchronized boolean saveUser(User user) {
        return db.saveUser(user);
    }


    /**
     * Getter for instance of the DatabaseManager.
     *
     * @return instance of database manager
     */
    public static DataBaseManager getInstance() {
        if (dbm == null)
            dbm = new DataBaseManager();
        return dbm;
    }

    /**
     * Getter for post. Get the post identified with the ID passed
     *
     * @param postID The id of the post in a string
     * @return Post object referred by the ID given
     */
    public Post getPost(String postID) {
        return db.getPost(postID);
    }

    /**
     * Save the post to the database. The result of this is given by the return value.
     * The database may reject to save your post [Not implemented yet] depending on the
     * criteria for it to save money
     *
     * @param post
     * @return
     */
    public boolean savePost(Post post) {
        return db.savePost(post);
    }

    /**
     * Get posts ordered by the time stamp. This might not always reflect the actual time
     * of the creation of the post. Because it is directly read from the db. But considering that our
     * Database will delete entries that get old. This should be sufficient. However it is possible
     * to have a very high rate of post creation thereby flooding the database. Solutions to this are
     * welcome. Presently I stand with an idea of controlling the rate of post creation using a requirement
     * for hash collisions for every post. This is the reason Post incliudes a nonce field.
     *
     * @param user  User object
     * @param limit limit on number of entries requested from the user
     * @return List containing the posts created by the user
     */
    public List<Post> getRecentPostsFromUser(User user, int limit) {
        return db.getRecentPostsFromUser(user, limit);
    }


    /**
     * Same as get recent posts by user. But this is get recent posts globally. This is the reply sent to
     * MESSAGE_REQ_UPDATE message.
     *
     * @param limit the number of entries limit the response to
     * @return List containing post objects
     */
    public List<Post> getRecentPosts(int limit) {
        return db.getRecentPosts(limit);
    }

    /**
     * Same as get recent posts but limited by a timestamp instead of number of posts
     *
     * @param limit the time of creation to limit the response to
     * @return
     */
    public List<Post> getRecentPosts(Timestamp limit) {
        return db.getRecentPosts(limit);
    }

    /**
     * Get users with affinity to the the target Identifier
     * @param identity  Object of type Identity
     * @param limit     Limit to number of users to get
     * @return          List of users if any found, null otherwise
     */
    public List<User> getUsersWithAffinityTo(Identity identity, int limit){
        return db.getUsersWithAffinityTo(identity, limit);
    }

}