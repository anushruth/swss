package edu.psu.hbg.turing.comp512;

import edu.psu.hbg.turing.comp512.Communication.Comms;
import edu.psu.hbg.turing.comp512.Communication.CommunicationError;
import edu.psu.hbg.turing.comp512.Config.Configuration;
import edu.psu.hbg.turing.comp512.Config.ConfigurationError;
import edu.psu.hbg.turing.comp512.DataBase.DataBaseManager;
import edu.psu.hbg.turing.comp512.DataBase.DefaultImplementation.InMemHashMap;
import edu.psu.hbg.turing.comp512.Identifier.Node;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Platform.PlatformError;

import static java.lang.Thread.sleep;

/**
 * Created by regnarts on 3/9/17.
 */
public class Main {
    private static final String nodeName = "athena";
    public static void main(String args[]){

        try {
            Configuration.init("config.xml");
            Comms.getInstance().startServer();
	        DataBaseManager.init(new InMemHashMap());
            Node node = Configuration.getInstance().loadNodeIdentity(nodeName);
            if(node==null){
                node = Configuration.getInstance().createNewNodeIdentity(nodeName,"public","RSA",1024,"SHA256WithRSA",
                        "SHA-256","password".toCharArray());
            }
            Configuration.getInstance().setNodeIdentity(node);
	    while(true) sleep(100000);
        } catch (ConfigurationError configurationError) {
            configurationError.printStackTrace();
        } catch (PlatformError platformError) {
            platformError.printStackTrace();
        } catch (CommunicationError communicationError) {
            communicationError.printStackTrace();
        } catch (InterruptedException e) {
            Log.E("Interruped exception: "+ e.getMessage());
        }
    }
}
