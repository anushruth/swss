package edu.psu.hbg.turing.comp512.Platform.DefaultImplementation;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import edu.psu.hbg.turing.comp512.Config.Configuration;
import edu.psu.hbg.turing.comp512.Config.ConfigurationError;
import edu.psu.hbg.turing.comp512.DataBase.Post;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Platform.PlatformBase;
import edu.psu.hbg.turing.comp512.Platform.PlatformError;
import sun.security.tools.keytool.CertAndKeyGen;
import sun.security.x509.X500Name;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.util.Arrays;

/**
 * @author Anushruth
 *
 * Implemetation of platform specific features for Nix. This is very similar to Linux and TODO: dBSD
 */
public class Nix implements PlatformBase.Platform {
    private static Nix instance = null;
    private static String privateKeyStoreSuffix = "priv_keystore.p12";
    private static String certKeyStoreSuffix = "cert_keystore.p12";



   // private static X509Certificate certificate = null;
    //private static String userCertificate = null;

    public static Nix getInstance() {
        if (instance == null)
            instance = new Nix();
        return instance;
    }

    private String certificateToString(Certificate cert) {
        StringWriter sw = new StringWriter();
        try {
            sw.write("-----BEGIN CERTIFICATE-----\n");
            //sw.write(DatatypeConverter.printBase64Binary(cert.getEncoded()).replaceAll("(.{64})", "$1\n"));
            sw.write(getBase64Encoded(cert.getEncoded()).replaceAll("(.{64})", "$1\n"));
            sw.write("\n-----END CERTIFICATE-----\n");
        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        }
        return sw.toString();
    }



    /**
     * Creates a new User Object to be used in the communication. Care to be taken on passing password because of generational garbage
     * collectors it is not guaranteed that the object dereference and garbage collection has occurred. PrivateKeys destroy method does
     * not work as the implementation is usually based on BigInt and the BigInt objects are immutable. The object is derefrenced as soon
     * as possible but there is no guarantee that it has been removed from memory. The private keys for user are encrypted ans stored in
     * ~/.swss/private_keys. The public certificates for the user are at ~/.swss/certificates. The private keys are named in the following
     * format userName.userGroup.userID.priv_keystore.pk12. Access to these is protected by a password. The certificates are named as
     * userID.userGroup.cert_keystore.pk12.
     *
     * @param name          User name to be used to generate the certificate
     * @param groupType     Type of group to be generating certificate for
     * @param city          City specified by the user
     * @param state         State specified by the user
     * @param country       2 Character country code of users residence as specified by user
     * @param password      Password to encrypt the storage
     * @return              User Object to be used in further communication
     * @throws PlatformError    Thrown on any errors occurring in creating the user
     *
     * TODO: Cleanup on failure
     */
    @Override
    public byte[] createKeyPairs(String algo, int keySize, String signatureAlgo, String digestAlgo, String name, String groupType, String city, String state, String country, char[] password) throws PlatformError {
        byte[] id;
        KeyStore privateKeyStore, certKeyStore = null;
        CertAndKeyGen gen;
        Certificate certificate = null;
        String userCertificate = null;
        File certFile = new File(System.getProperty("user.home")+"/.swss/certificates/"+name+"."+certKeyStoreSuffix);
        if(certFile.exists()){
            try {
                certKeyStore = KeyStore.getInstance("PKCS12");
                certKeyStore.load(new FileInputStream(certFile), password);
                certificate = certKeyStore.getCertificate(name + "." + groupType + ".cert");
            } catch (Exception ex) {
                throw new PlatformError(ex, "Error opening certificate store: "+ ex.getMessage());
            }
        }
        if(certificate != null){
            throw new PlatformError(name +" Certificate for "+ groupType +" already exists");
        }
        try{
            privateKeyStore = KeyStore.getInstance("PKCS12");
            privateKeyStore.load(null, null);
            gen = new CertAndKeyGen(algo, signatureAlgo);
            gen.generate(keySize);
            RSAPrivateKey privateKey = (RSAPrivateKey) gen.getPrivateKey();
            certificate = gen.getSelfCertificate(new X500Name(name, groupType, Configuration.getInstance().getApplicationName(), city, state, country), 1096 * 24 * 60 * 60);
            userCertificate = certificateToString(certificate);
            MessageDigest digest = MessageDigest.getInstance(digestAlgo);
            digest.update(userCertificate.getBytes());
            id = digest.digest();
            String idString = DatatypeConverter.printHexBinary(id);
            X509Certificate[] chain = new X509Certificate[1];
            chain[0] = (X509Certificate) certificate;
            privateKeyStore.setKeyEntry(idString + "." +groupType + ".private", privateKey, password, chain);
            new File(System.getProperty("user.home")+"/.swss/private_keys").mkdirs();
            privateKeyStore.store(new FileOutputStream(System.getProperty("user.home")+"/.swss/private_keys/"+name +"."+ groupType + "."+ idString + "."  + privateKeyStoreSuffix), password);
            if(certKeyStore!=null){
                certKeyStore.setCertificateEntry(name + "." + groupType + ".cert", certificate);
                certKeyStore.store(new FileOutputStream(certFile),password);
            }else{
                privateKeyStore.deleteEntry(idString + "." + groupType + ".private");
                privateKeyStore.setCertificateEntry(name + "." + groupType + ".cert", certificate);
                new File(System.getProperty("user.home") + "/.swss/certificates").mkdirs();
                privateKeyStore.store(new FileOutputStream(certFile), password);
            }
            Arrays.fill(password,' ');
        }catch (Exception ex){
            ex.printStackTrace();
            throw new PlatformError(ex, "Error creating keys: "+ex.getMessage());
        }
        if(userCertificate != null)
            return userCertificate.getBytes();
        return null;
    }

    @Override
    public void signPost(Post post, char[] password) throws ConfigurationError {
        try {
            KeyStore privateKeyStore = KeyStore.getInstance("PKCS12");
            privateKeyStore.load(new FileInputStream(System.getProperty("user.home")+"/.swss/private_keys/"+ post.getOrigin().getName() +"."+ post.getOrigin().getGroup() + "."+ post.getOrigin().getIDString() + "."  + privateKeyStoreSuffix), password);
            PrivateKey privateKey = (PrivateKey) privateKeyStore.getKey(post.getOrigin().getIDString() + "." + post.getOrigin().getGroup() + ".private", password);
            Signature sign = Signature.getInstance(post.getOrigin().getSignatureAlgo());
            sign.initSign(privateKey);
            sign.update(post.getMarshaledPost().toString().getBytes());
            post.getMarshaledPost().put("Signature", byteToString(sign.sign()));
            MessageDigest digest = MessageDigest.getInstance(post.getOrigin().getDigestAlgo());
            digest.update(post.getMarshaledPost().toString().getBytes());
            post.getMarshaledPost().put("Post ID", byteToString(digest.digest()));
            Arrays.fill(password,' ');
        } catch (IOException e) {
            throw new ConfigurationError(e, "Error locating key to sign " + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            throw new ConfigurationError(e, "Algorithm not found " + e.getMessage());
        } catch (CertificateException e) {
            throw new ConfigurationError(e, "Certificate Error " + e.getMessage());
        } catch (KeyStoreException e) {
            throw new ConfigurationError(e, "Error in key store " + e.getMessage());
        } catch (UnrecoverableKeyException e) {
            throw new ConfigurationError(e, "Cannot recover key from key store " + e.getMessage());
        } catch (InvalidKeyException e) {
            throw new ConfigurationError(e, "Invalid key " + e.getMessage());
        } catch (SignatureException e) {
            throw new ConfigurationError(e, "Signature error " + e.getMessage());
        }
    }

    @Override
    public void verifyPost(Post post) throws PlatformError, ConfigurationError {
        PublicKey publicKey = post.getOrigin().getCertificate().getPublicKey();
        byte[] userID = post.getOrigin().getID();
        try {
            MessageDigest digest = MessageDigest.getInstance(post.getOrigin().getDigestAlgo());
            digest.update(((String)post.getOrigin().getCertificateString()).getBytes());
            if(!Arrays.equals(userID,digest.digest())){
                throw new PlatformError("Cannot verify user, certificate doesn't match");
            }
            Signature sign = Signature.getInstance(((X509Certificate)post.getOrigin().getCertificate()).getSigAlgName());
            sign.initVerify(publicKey);
            byte[] signature = stringToByte((String)post.getMarshaledPost().get("Signature"));
            post.getMarshaledPost().remove("Signature");
            post.getMarshaledPost().remove("Post ID");
            sign.update(post.getMarshaledPost().toString().getBytes());
            if(!sign.verify(signature))
                throw new PlatformError("Signature Verification failed. Post not valid");
        } catch (SignatureException e) {
            throw new PlatformError(e, "Signature error " + e.getMessage());
        } catch (InvalidKeyException e) {
            throw new PlatformError(e, "Invalid Key " + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            throw new PlatformError(e, "Algorithm not Found " + e.getMessage());
        }
    }

    @Override
    public boolean verifyCertificate(byte[] hash, Certificate certificate, String digestAlgorithm) {
        try {
            MessageDigest digest = MessageDigest.getInstance(digestAlgorithm);
            digest.update(certificateToString(certificate).getBytes());
            if (!Arrays.equals(hash, digest.digest())) {
                Log.I("Coudln't verify " + DatatypeConverter.printHexBinary(hash) + " with certificate \n" + certificateToString(certificate));
                return false;//Will fix later
            }
            return true;
        } catch (NoSuchAlgorithmException e) {
            Log.I("Algorithm not Found " + e.getMessage());
            return false;
        }
    }

    @Override
    public String getCertificateStringFromCertificate(Certificate certificate) {
        return certificateToString(certificate);
    }

    @Override
    public Certificate getCertificateFromCertificateString(String certificate) throws PlatformError {
        try {
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            return certificateFactory.generateCertificate(new ByteArrayInputStream(certificate.getBytes()));
        } catch (CertificateException e) {
            throw new PlatformError(e,"Unable to parse certificate "+ e.getMessage());
        }
    }

    @Override
    public String getBase64Encoded(byte[] input) {
        return Base64.encode(input);
    }

    @Override
    public String byteToString(byte[] input) {

        return DatatypeConverter.printHexBinary(input);
    }

    @Override
    public byte[] stringToByte(String input) {
        return DatatypeConverter.parseHexBinary(input);
    }

}
