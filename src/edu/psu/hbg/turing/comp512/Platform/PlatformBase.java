package edu.psu.hbg.turing.comp512.Platform;

import edu.psu.hbg.turing.comp512.Config.Configuration;
import edu.psu.hbg.turing.comp512.Config.ConfigurationError;
import edu.psu.hbg.turing.comp512.DataBase.Post;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Identifier.User;
import edu.psu.hbg.turing.comp512.Platform.DefaultImplementation.Nix;

import java.lang.String;
import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Arrays;

/**
 * <h1>PlatformBase</h1>
 * <p>
 * This abstracts all the platform specific features from rest of the framework.
 * The Platform interface defined therein is supposed to be implemented for all the platforms. Presently
 * all the Unix based platforms use {@link Nix} class. If there are any changes to be found in any of
 * the platforms it can be changed to a new implementation. It is also possible to use the library without
 * any default implementation of the platform class by explicitly passing the implementation to the
 * {@link PlatformBase#init(Platform)} before calling {@link PlatformBase#getInstance()} for the first time.</p>
 *
 * @author Anushruth
 * @version 0.1
 */
public class PlatformBase {

    /**
     * Private instance variables handling instances
     */
    private static PlatformBase.Platform platform = null;
    private static PlatformBase instance = null;

    /**
     * Initialization of {@link Platform}. Passing a new implementation of platform will prevent the
     * {@link PlatformBase} from instantiating a default one
     * @param platform          Implementation of {@link Platform} interface
     * @throws PlatformError    Thrown if platform is already instantiated.
     */
    public static void init(Platform platform) throws PlatformError {
        if(platform==null){
            PlatformBase.platform = platform;
        }else
            throw new PlatformError("Platform base is already set");
    }

    /**
     * <h1>Platform</h1>
     * <p>Interface for all implementations of platform. Contains the list of methods that all the implementations for a
     * platform must create.</p>
     *
     * @author Anushruth
     * @version 0.1
     */
    public interface Platform{
        /**
         * Function to create key pairs.
         * @param algo              Name of the public cipher algorithm to be used
         * @param keySize           Key size of the cipher
         * @param signatureAlgo     Algorithm to be used for signing
         * @param digest            Hash algorithm to create an ID {@see User}
         * @param name              Name of the certificate
         * @param groupType         Group type of the account
         * @param city              City specified by the user
         * @param state             State specified by the user
         * @param country           2 Character country code specified by the user
         * @param password          Password. Will be reset to blanks after use
         * @return                  Raw public certificate
         * @throws PlatformError    Any errors resulting therein (Errors can be implementation specific)
         */
        byte[] createKeyPairs(String algo, int keySize, String signatureAlgo, String digest, String name, String groupType, String city, String state, String country,  char[] password) throws PlatformError;

        /**
         * Function to sign the post with the private key of the user
         * @param post                  Post instance
         * @param password              Password, Will be reset to blanks after use
         * @throws ConfigurationError   Any errors resulting therin (Errors can be implementation specific
         */
        void signPost(Post post, char[] password) throws ConfigurationError;

        /**
         * Function to verify the integrity of the post. It throws platform error if the post is not verifiable
         * TODO: change to boolean? instead of throwing error. Its better and platform error doesn't make sense
         * @param post Post to be saved
         * @throws PlatformError        Post not verifiable or some other error
         * @throws ConfigurationError   Some error with configuration manager
         */
        void verifyPost(Post post) throws PlatformError, ConfigurationError;

        /**
         * Verify the hash of the given certificate. Check if the user ID is what it claims to be
         * @param hash              byte array containing the hash of the certificate
         * @param certificate       certificate associated with the user
         * @param digestAlgorithm   digest used to generate user ID. (I think it will crash if anything other than SHA-256 is passed ??)
         * @return
         */
        boolean verifyCertificate(byte[] hash, Certificate certificate, String digestAlgorithm);

        /**
         * Get a string representation of the certificate that can be saved to the disk or transmitted
         * @param certificate   Certificate object to be converted
         * @return              String containing the certificate
         */
        String getCertificateStringFromCertificate(Certificate certificate);

        /**
         * Generate a certificate object from the string
         * @param certificate  String containing the representation of the certifacte
         * @return  Certificate object represented by the string
         * @throws PlatformError    Thrown on error in conversion
         */
        Certificate getCertificateFromCertificateString(String certificate) throws PlatformError;

        /**
         * Get Base64 encoded representation of the byte array. This changes based on the system being used. It depends on
         * the libraries available therefore this function is supposed to be implemented as a platform base
         * @param input Byte array containing the data to be represented
         * @return  string representation of the byte array input
         */
        String getBase64Encoded(byte[] input);

        /**
         * Convert to string representation of the byte array. This returns the hex representation of the byte array
         * @param input byte array
         * @return  string representing the byte array
         */
        String byteToString(byte[] input);

        /**
         * Convert the string representation of the byte array back to byte array
         * @param input String containing the hex representation of the byte array
         * @return  byte array represented by the string
         */
        byte[] stringToByte(String input);
    }

    public static PlatformBase getInstance() {
        if(instance == null) {
            try {
                instance = new PlatformBase();
            } catch (ConfigurationError configurationError) {
                Log.E(configurationError.getMessage());
            } catch (PlatformError platformError) {
                Log.E(platformError.getMessage());
            }
        }
        return instance;
    }

    /**
     * TODO: Some day I will get a windows computer and implement a platform base for that. Hopefully !!
     * @throws ConfigurationError
     * @throws PlatformError
     */
    private PlatformBase() throws ConfigurationError, PlatformError{
        switch (Configuration.getHost()){
            case OS_LINUX:
            case OS_MAC:
            case OS_BSD:
                platform = Nix.getInstance();
                break;
            default:
                throw new PlatformError("Unsupported Platform");
        }
    }

    /**
     * Calls specific platform implementation to create new key pairs. Then creates an user object using that
     * Don't really use this. Because this doesn't save the generated user to config. The application has no
     * idea that this user credentials exist on the disk. Only way to know would be put up the credentials in
     * the config file. However createNewUser from Configuration class takes care of this. Use that instead
     *
     * @param name              User name to be used to generate the certificate
     * @param groupType         Type of group to be generating certificate for
     * @param city              City specified by the user
     * @param state             State specified by the user
     * @param country           2 Character country code of users residence as specified by user
     * @param password          Password to encrypt the storage
     * @return                  User Object to be used in further communication
     * @throws PlatformError    Thrown on any errors occurring in creating the user
     */
    public User createNewUser(String name, String groupType, String city, String state, String country,
                              String cipher, String digestAlgo, String signature, Integer keyLength,
                              char[] password ) throws PlatformError, ConfigurationError {
        User user = null;
        MessageDigest digest = null;
        CertificateFactory cf = null;
        digestAlgo = digestAlgo==null?"SHA-256":digestAlgo;
        keyLength = keyLength==null?4096:keyLength;
        signature = signature==null?"SHA256WithRSA":signature;
        cipher = cipher==null?"RSA":cipher;

        Configuration config = Configuration.getInstance();
        try {
            digest = MessageDigest.getInstance(digestAlgo);
            byte[] cert = platform.createKeyPairs(cipher, keyLength, signature, digestAlgo, name, groupType, city, state, country, password);
            digest.update(cert);
            cf = CertificateFactory.getInstance("X.509");
            user = new User(name , groupType, digest.digest(), digestAlgo, signature, cf.generateCertificate(new ByteArrayInputStream(cert)));
        } catch (NoSuchAlgorithmException e) {
            throw new PlatformError(e,"Hash error: "+ e.getMessage() +" Please check config");
        }catch (CertificateException e) {
            throw new PlatformError(e,"Certificate Error: "+ e.getMessage());
        }
        Arrays.fill(password,' ');
        return user;
    }

    /**
     * Function to create new Identity. It is not required that a different identity be created for a node. User identity can be used.
     * But it adds to the security if you are trying to hide you IP location this might be helpful. However I have no clue about how effective
     * this is. But the functionality exists. If willing you can use it
     * @param algo              Algorithm to use for generating certificate. ex, RSA
     * @param keySize           KeySize for the public/private key generation
     * @param signatureAlgo     Signature algorithm to be used
     * @param digestAlgo        digest algorithm to be used. Please only use "SHA-256"
     * @param name              Name of the user
     * @param groupType         Group type of the user. [Haven't really determined how to use this]
     * @param city              City of residence
     * @param state             State
     * @param country           Country of residence. Two character representation of the country name. Ex US
     * @param password          password to encrypt the keystore and certificate
     * @return                  byte array representing the ID created by the digest algo given
     * @throws PlatformError    Thrown by platform on any errors in the implementation
     */
    public byte[] createNodeIdentity(String algo, int keySize, String signatureAlgo, String digestAlgo, String name, String groupType, String city,
                                     String state, String country, char[] password) throws PlatformError {
        return platform.createKeyPairs(algo,keySize,signatureAlgo,digestAlgo,name,groupType,city,state,country,password);
    }

    /**
     * Sign the post with the password for the user certificate
     * @param post          Post object to be signed
     * @param password      password to extract the private key from keystore
     * @throws ConfigurationError   thrown on error by any underlying modules
     */
    public void signPost(Post post, char[] password) throws ConfigurationError {
        platform.signPost(post,password);
    }

    /**
     * Verify the integrity of the post by checking the signature
     * @param post          Post to be checked
     * @throws PlatformError        Thrown on errors by the platform implementation or verification failure
     * @throws ConfigurationError   Thrown on errors by the configuration implementation
     */
    public void verifyPost(Post post) throws PlatformError, ConfigurationError {
        platform.verifyPost(post);
    }

    /**
     * Conversion routines used to generate the certificate object from the string
     * @param certificate certificate object to be converted
     * @return  String containing the certificate represnetation
     */
    public String getCertificateStringFromCertificate(Certificate certificate){
        return platform.getCertificateStringFromCertificate(certificate);
    }


    /**
     * Generate a certificate object from the string
     * @param cert  String containing the representation of the certifacte
     * @return  Certificate object represented by the string
     * @throws PlatformError    Thrown on error in conversion
     */
    public Certificate getCertificateFromCertificateString(String cert) throws PlatformError {
        return platform.getCertificateFromCertificateString(cert);
    }

    /**
     * Get Base64 encoded representation of the byte array. This changes based on the system being used. It depends on
     * the libraries available therefore this function is supposed to be implemented as a platform base
     * @param input Byte array containing the data to be represented
     * @return  string representation of the byte array input
     */
    public String getBase64Encoded(byte[] input){
        return platform.getBase64Encoded(input);
    }

    /**
     * Verify the certificate. Basically check the hash of the certificate to see if the certificate is from the same person
     * @param hash              Hash of the certificate claimed
     * @param certificate       Certificate object
     * @param digestAlgorithm   digest algorithm used to generate this hash. [Please only use SHA-246 for now]
     * @return
     */
    public boolean verifyCertificate(byte[] hash, Certificate certificate, String digestAlgorithm){
        return platform.verifyCertificate(hash, certificate, digestAlgorithm);
    }

    /**
     * Convert to string representation of the byte array. This returns the hex representation of the byte array
     * @param input byte array
     * @return  string representing the byte array
     */
    public String byteToString(byte[] input){ if (input!=null) return platform.byteToString(input); else return null;}

    /**
     * Convert the string representation of the byte array back to byte array
     * @param input String containing the hex representation of the byte array
     * @return  byte array represented by the string
     */
    public byte[] stringToByte(String input){ return platform.stringToByte(input);}

}
