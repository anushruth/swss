package edu.psu.hbg.turing.comp512.Platform;

/**
 * Created by regnarts on 3/3/17.
 */
public class PlatformError extends Exception{
    public PlatformError(String msg){
        super(msg);
    }
    public PlatformError(Exception ex, String message){ super(message, ex);}
}
