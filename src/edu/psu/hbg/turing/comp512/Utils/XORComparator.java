package edu.psu.hbg.turing.comp512.Utils;

import edu.psu.hbg.turing.comp512.Identifier.Identity;

import java.util.Comparator;

/**
 * @author Anushruth
 * @version 0.1
 *          <p> This is implemented to facilitate the XOR routing facility. The routes are compared to a target node
 *          Which is input through the constructor. This is used to initialize the PriorityQueue which is then used
 *          to arrange the nodes in their least XOR distance order</p>
 */
public class XORComparator implements Comparator<Identity> {
    private byte[] targetAddress;
    private boolean inverse=false;

    /**
     * Constructor with target node input
     *
     * @param identity {@link Identity} object containing target address
     */
    public XORComparator(Identity identity) {
        this.targetAddress = identity.getID();
        if (targetAddress == null)
            throw new NullPointerException("Target Address cannot be null");
    }

    /**
     * Constructor with target node input
     *
     * @param address byte array containing target address
     */
    public XORComparator(byte[] address) {
        this.targetAddress = address;
        if (targetAddress == null)
            throw new NullPointerException("Target Address cannot be null");
    }

    /**
     * Function to compare two nodes with their XOR distance to the target node.
     *
     * @param o1 Node one
     * @param o2 Node two
     * @return int referring to the distance difference between the two.
     */
    @Override
    public int compare(Identity o1, Identity o2) {
        byte[] element1 = o1.getID();
        byte[] element2 = o2.getID();
        int distance1 = getDistanceBetween(targetAddress,element1);
        int distance2 = getDistanceBetween(targetAddress,element2);
        if(!inverse)
            return distance2 - distance1;
        return distance1-distance2;
    }

    public XORComparator reverse(){
        inverse = true;
        return this;
    }

    public static int getDistanceBetween(byte[] targetAddress, byte[] originAddress){
        int distance1 = 0;
        for (int i = 0; i < originAddress.length; i++) {
            int res = (((0xff) & targetAddress[i]) ^ ((0xff) & originAddress[i]));
            if (res == 0)
                distance1 += 8;
            else {
                for (i = 1; i < 8; i++) {
                    if (res >> (i) != 0) {
                        distance1++;
                    } else
                        break;
                }
                break;
            }
        }
        return distance1;
    }
}
