package examples.swss;

import edu.psu.hbg.turing.comp512.Communication.Comms;
import edu.psu.hbg.turing.comp512.Communication.CommunicationError;
import edu.psu.hbg.turing.comp512.Config.Configuration;
import edu.psu.hbg.turing.comp512.Config.ConfigurationError;
import edu.psu.hbg.turing.comp512.DataBase.DataBaseManager;
import edu.psu.hbg.turing.comp512.DataBase.DefaultImplementation.InMemHashMap;
import edu.psu.hbg.turing.comp512.DataBase.Message;
import edu.psu.hbg.turing.comp512.DataBase.Post;
import edu.psu.hbg.turing.comp512.Identifier.Node;
import edu.psu.hbg.turing.comp512.Identifier.User;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Platform.PlatformError;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;


/**
 * @author Anushruth
 * @version 0.1
 */
public class DumbServer {
    /**
     * This creates a server that only queries for an posts from user every 10 seconds and prints out the output
     * @param args
     */
    public static void main(String args[]){
        if((args.length != 2)) {
            System.out.println("Usage: Java -cp <swss.jar-path> DumbServer <user-name> <listen-to-ID>");
            return;
        }
        String targetNode = args[1];
        Log.SetFile("find_"+targetNode+".log");
        try {
            User user;
            if((new File(args[0]+"_config.xml")).exists()){
                Log.I("Config file exists");
                Configuration.init(args[0]+"_config.xml");
                user = Configuration.getInstance().loadUser(args[0]);
            }else {
                Log.I("Config file doesn't exist");
                Configuration.init(args[0]+"_config.xml");
                user = Configuration.getInstance().createNewUser(args[0], "test", "SunLab", "PA", "US", "RSA",
                        "SHA-256", "SHA256WithRSA", 1024, "password".toCharArray());
            }
            Configuration config = Configuration.getInstance();
            DataBaseManager.init(new InMemHashMap());
            DataBaseManager.getInstance().saveUser(user);
            Node node = config.loadNodeIdentity(user.getName());
            config.setNodeIdentity(node);
            Comms.init(0);
            Comms.getInstance().startServer();
            Comms.getInstance().updateNodes();
            List<String> nodeInfoReq = new ArrayList<>();
            nodeInfoReq.add(targetNode);
            Timestamp lastPost=null;
            Node target = Comms.getInstance().findNode(targetNode);
            while(true){
                Message msg = new Message(Message.MessageType.MESSAGE_REQ_UPDATE,null,null,null,null,null,null,10);
                Message reply = Comms.getInstance().sendMessageWaitForReply(msg, target);
                if(reply!=null&&reply.getPosts()!=null) {
                    for(User usr : reply.getUsers()){
                        DataBaseManager.getInstance().saveUser(usr);
                    }
                    for (Post post : reply.getPosts()) {
                        DataBaseManager.getInstance().savePost(post);
                    }
                }
                User targetUser = DataBaseManager.getInstance().getUser(targetNode);
                if(targetUser!=null) {
                    List<Post> recentPosts = DataBaseManager.getInstance().getRecentPostsFromUser(targetUser, 10);
                    System.out.println("Updated posts from " + targetUser.getName());
                    for (Post post : recentPosts) {
                        DataBaseManager.getInstance().saveUser(post.getOrigin());
                        DataBaseManager.getInstance().savePost(post);
                        if (lastPost != null && post.getCreatedOn().after(lastPost)) {
                            System.out.println("From: \t" + post.getOrigin().getName());
                            System.out.println("On:   \t" + post.getCreatedOn().toString());
                            System.out.println("Post: \t" + post.getPost());
                            lastPost = post.getCreatedOn();
                        } else {
                            lastPost = post.getCreatedOn();
                            System.out.println("From: \t" + post.getOrigin().getName());
                            System.out.println("On:   \t" + post.getCreatedOn().toString());
                            System.out.println("Post: \t" + post.getPost());
                        }
                        System.out.println("----------------------------------------------- ");
                    }
                    System.out.println("----------------------------------------------- " + targetUser.getName());
                    sleep(10000);
                }
            }
        } catch (ConfigurationError configurationError) {
            Log.E("Error in configuration: "+ configurationError.getMessage());
        } catch (PlatformError platformError) {
            Log.E("Platform Error: "+ platformError.getMessage());
        } catch (CommunicationError communicationError) {
            Log.E("Communcation error: "+ communicationError.getMessage());
        } catch (InterruptedException e) {
            Log.E("Interruped exception: "+ e.getMessage());
        }
    }
}
