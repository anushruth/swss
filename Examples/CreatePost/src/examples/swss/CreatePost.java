package examples.swss;

import edu.psu.hbg.turing.comp512.Communication.Comms;
import edu.psu.hbg.turing.comp512.Communication.CommunicationError;
import edu.psu.hbg.turing.comp512.Config.Configuration;
import edu.psu.hbg.turing.comp512.Config.ConfigurationError;
import edu.psu.hbg.turing.comp512.DataBase.DataBaseManager;
import edu.psu.hbg.turing.comp512.DataBase.DefaultImplementation.InMemHashMap;
import edu.psu.hbg.turing.comp512.DataBase.Message;
import edu.psu.hbg.turing.comp512.DataBase.Post;
import edu.psu.hbg.turing.comp512.Identifier.Node;
import edu.psu.hbg.turing.comp512.Identifier.User;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Platform.PlatformBase;
import edu.psu.hbg.turing.comp512.Platform.PlatformError;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Anushruth
 * @version 0.1
 */
public class CreatePost {

    /**
     * This program sends post to the network
     * @param args
     */
    public static void main(String args[]){
        if((args.length != 2)) {
            System.out.println("Usage: Java -cp <swss.jar-path> CreatePost <user-name> <post>");
            return;
        }
        Log.SetFile(args[0]+".log");
        try {
            User user;
            if((new File(args[0]+"_config.xml")).exists()){
                Log.I("Config file exists");
                Configuration.init(args[0]+"_config.xml");
                user = Configuration.getInstance().loadUser(args[0]);
            }else {
                Log.I("Config file doesn't exist");
                Configuration.init(args[0]+"_config.xml");
                user = Configuration.getInstance().createNewUser(args[0], "test", "SunLab", "PA", "US", "RSA",
                        "SHA-256", "SHA256WithRSA", 1024, "password".toCharArray());
            }
            System.out.println("Using ID: "+ user.getIDString());
            Configuration config = Configuration.getInstance();
            DataBaseManager.init(new InMemHashMap());
            Node node = config.loadNodeIdentity(user.getName());
            config.setNodeIdentity(node);
            Comms.init(0);
            Comms.getInstance().startServer();
            Comms.getInstance().updateNodes();
            List<User> users = new ArrayList<>();
            users.add(user);
                List<Post> prevPost = DataBaseManager.getInstance().getRecentPostsFromUser(user, 1);
                Post post = Post.newPost(user, PlatformBase.getInstance().stringToByte("DEADBEEF"), args[1],
                        (prevPost!=null)&&(prevPost.size()>0)?prevPost.get(0).getPostID():"FirstPost","password".toCharArray());
                PlatformBase.getInstance().signPost(post, "password".toCharArray());
                Log.I("Creating new post "+post.getPost());
                DataBaseManager.getInstance().savePost(post);
                prevPost.add(post);
                Message msg = new Message(Message.MessageType.MESSAGE_UPDATE,users,prevPost,null,null,null,null,null);
                Comms.getInstance().sendMessage(msg,user); //Send message to all nodes nearby
        } catch (ConfigurationError configurationError) {
            Log.E("Error in configuration: "+ configurationError.getMessage());
        } catch (PlatformError platformError) {
            Log.E("Platform Error: "+ platformError.getMessage());
        } catch (CommunicationError communicationError) {
            Log.E("Communcation error: "+ communicationError.getMessage());
        }
    }

}
