package examples.swss;

import edu.psu.hbg.turing.comp512.Communication.Comms;
import edu.psu.hbg.turing.comp512.Communication.CommunicationError;
import edu.psu.hbg.turing.comp512.Config.Configuration;
import edu.psu.hbg.turing.comp512.Config.ConfigurationError;
import edu.psu.hbg.turing.comp512.DataBase.DataBaseManager;
import edu.psu.hbg.turing.comp512.DataBase.DefaultImplementation.InMemHashMap;
import edu.psu.hbg.turing.comp512.Identifier.Node;
import edu.psu.hbg.turing.comp512.Identifier.User;
import edu.psu.hbg.turing.comp512.Log.Log;
import edu.psu.hbg.turing.comp512.Platform.PlatformError;

import java.io.File;

/**
 * Example implementation of a Node in Java
 *
 * @author Anushruth
 * @version 0.1
 */
public class NaiveServer {
//    This was used in testing setup to randomly query nodes. And is just the identifiers related to the setup created to test the nodes
//    private static final String[] IDList = {
//            "37C47E233035A02DB84FA268EE85545B1B7E44C5549FDC01F45395BA4981E0D3",
//            "3EC4EDC1C7E3E2703E39427CF04C4837A91721E1187DF0E9C7A9F52248F48B68",
//            "9B13899C8515AEF4B4E5AFD6C763B61482B4FB370E98AAB7E1145269347CC56D",
//            "1F67B406C331FB1269F54A693A361D66BE1287F59557A37BC33EE9F20AD65F30",
//            "7BBD8DC0FD28C216F1DCE863AD96194CE6290770FB42C8BE34CD1F3730E2178C",
//            "51C8C6DEA4E1E787277D4F2CDCD835B1D66851F37CB7A20A80B3B5FE682C30C8",
//            "889E7F4718026AC27BB1CC08A78168F1B4D1DFB7C5C0141BAC4117BA15F6E148",
//            "49C08A906EDCE1F527D46FF5A7CE6DCDAAA58A55F112421FD49D69195E8AC75B",
//            "1BC54EE280D914590A9F5D7447FAF9518E26175881B55DDDEDC38AE1A494B68F",
//            "3714EF35E2AD456C45A9AB2CCA96CED3DE85C55637F86CE60CF96E93A45058E8",
//            "5E389042216DBEC601EE31934E7929AF721E3946B597DD5341AA4A6089F0B104",
//            "19D36D01C85FF61768A00DE5FD2CBB4978EC5D7523B27AF17D7478A889CA7076",
//            "D5B85DE12306D4D92C026E1A9751E49539F7D67063082825EAA4F7B51B93FBD3",
//            "522ADC0FF17CB2DF556A18213823761495D0F7AA87AA8ADF4ABA4DE604D10BA0",
//            "9BE0DF2D140DB39B38ACC7B44B51BA035E756BA28E432F418D761F58AFADA216",
//            "2DDF005702069A06EFDB4DDCE756A77A805208E39239866530051CFED79E146D",
//            "650074DDDEEB0AAFFF588FFF4730C888C361FA5E970E38358340539BAB36CEA8",
//            "9D523025D1563D160D00ABFC0DE80CBDDA00FD9F3BCE3499FF9FCE86E230437A",
//            "54837F8A2A479DF885B48422BC78764187FFF46E62CF3366E9826E15FF51984A",
//            "8940682D6EC10258A0A3D420D46565EF1B2EF7E688BBF334DD824807FA8CDE54",
//            "DF02A9ABA0C0B1D72D82C8590F98B65F7CC142A2F2D13AA380D9C399349D8670",
//            "ABA8E8DCD87E1BD446C65D5C44E841BA0CA33BC9A838CE78DE4FDB6ACB74B3B8",
//            "FE2E5F60A65CA606DF61E604FDE4B703EE8C9653BE9A28FD897F5E1911E19418",
//            "238799849BF3C06E9502FEFF5702C4DA36745C1216F364CD8D51C3B48CFB5407",
//            "B509292C9994230C87288C71A64DEF5BC74074AF90B8E798E21735462B7C6BBD",
//            "D5B40A0C88C1C7EF92A713B08E92789739B1199C361F9191D018CCE2127D15BF",
//            "7D4F5A0209F86684F65BCCF13DFE9171BE62AB4836B74CB1553066A94F481D88",
//            "C5EE93FE86B1E1C50E69EC4DA36D9D89B83AF163EDE152267D52460D84BE53BE",
//            "15CA08DDE78A519A3388F88B940E1D4E9903616D3DB254A146DF294DA6862402",
//            "CCEB27F81BB8E22E4D07A3CB3EF819CB0B9842951D9D7295780836CD336DF1F6",
//            "9061FC44BB061646C7AE9F87BE5B9F04E2D0A93D6AE3474FD7543D5666C2BA60",
//            "4C515175E953503DF123192E4350BCF0CD67A92E4FE5DA402349B8A1296B0AB5",
//            "26A325F80F86AE51397B09181F34A6F6A87C7270B0FEC9BAE406D2172BD35AC9",
//            "355AAC2A76DE01969A3D50C731FF4BDE0296F603D45143061032B6E7D38DC25A",
//            "A24277BCECED639D6969AACA46C98DBB35D956954D799336EBA6363BF2287065"
//    };

    /**
     * Creates a new user with given name if not available. And then queries for the given node.
     * Please delete the [name]_config.xml and ~/.swss directory if new identity is desired.
     * @param args
     */
    public static void main(String args[]){
//        Used for testing purposes. Uncomment if log file is desired to be saved to disk. Without this all output is put on to stdout
//        Log.SetFile("SWSS_"+args[0]+".log");
        if(!(args.length > 0)) {
            System.out.println("Usage: Java -cp <swss.jar-path> NaiveServer <user-name> <ID-to-be-queried>");
            return;
        }
        try {
            User user;
            if((new File(args[0]+"_config.xml")).exists()){
                Log.I("Config file exists");
                Configuration.init(args[0]+"_config.xml");
                user = Configuration.getInstance().loadUser(args[0]);
            }else {
                Log.I("Config file doesn't exist");
                Configuration.init(args[0]+"_config.xml");
                user = Configuration.getInstance().createNewUser(args[0], "test", "SunLab", "PA", "US", "RSA",
                        "SHA-256", "SHA256WithRSA", 1024, "password".toCharArray());
            }
            Configuration config = Configuration.getInstance();

            DataBaseManager.init(new InMemHashMap());
            Node node = config.loadNodeIdentity(user.getName());
            config.setNodeIdentity(node);
            Comms.init(0);
            Comms.getInstance().startServer();
            Comms.getInstance().findNode(args[1]);
//            Test code, Uncomment if to be used for testing
//            Random random = new Random(System.currentTimeMillis());
//            while(true) {
//                if (Comms.getInstance().findNode(IDList[random.nextInt(IDList.length-1)]) != null)
//                    Log.I("Found it !!");
//            }
            Comms.getInstance().stopServer();
        } catch (ConfigurationError configurationError) {
            configurationError.printStackTrace();
        } catch (PlatformError platformError) {
            platformError.printStackTrace();
        } catch (CommunicationError communicationError) {
            communicationError.printStackTrace();
        }
    }
}
