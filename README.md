SWSS

Started by Anushruth 

This is a backend library built to handle all the communication and messaging. This does not do anything by itself, but will have to be included the application
to be built in order to use the features provided. It uses JSON library from . The library is copied completely along with code and compiled into the library so
there is no need to download and add an extra library. It has default implementations tested on 'Mac', 'Linux', 'BSD'. Any other implementation can be 
specified during run time without having to recompile the library.

Build

Java 8 is required to build. Both Open JDK or Sun JDK should be fine. Install the JDK and then edit kadmelia_base.properties to reflect the jdk path in order to build
The library can be built with apache-ant. Install ant on your system and run

    ant -f kadmelia_base.xml
    
to build the library. The library is built into a Jar file. This can then be imported into your project and used.

If you are interested in examining the library itself IntelliJ Idea is the best IDE to be used. Because the repo also contains the configuration files for that
it can easily be imported into the IDE. If you wish to use any other IDE there is a ant file included which can be used to build the project