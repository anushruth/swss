SWSS

CONFIGURATION
=============

Configuration is persisted by storing into an xml file. This is done by Configuration class in java. This document lists the tags used in the configuration xml and the meaning of their value.

### <SWSS> ###
This is the top level tag that encompasses all the information containing the configuration. Any info that is not present under this tag will not be read by the library.

### <APPLICATION> ###
The attributes of this tag encompass the elements required for the application to identify itself. There is only one instance of this tag. If multiple instances are found the first one is used.

### <NODE> ###
The attributes of this tag describe the node identity of the application. Note that there can be multiple nodes and the application can decide the node to be used.

### <USER> ###
The attributes of this tag describe the user identity of the available profiles.


The following are the attributes that any of these tags might carry depending on the type of the tag.

Attributes
***

    * NAME
         Name attribute
    * ID
        Identification, usually a hash of the public certificate
    * ALGORITHM
        Cipher algorithm, String representation as used in Java crypto libraries ex. RSA
    * DIGEST
        Hash algorithm, String representation as used in Java crypto libraries ex. SHA-256
    * SIGNATURE
        Signature algorithm, String representation as used in Java crypto ex. SHA256WithRSA
    * KEY_LENGTH
        Key length for the cipher where applicable ex 4096
    * CERTIFICATE
        Public key self signed certificate in X.509 format
    * GROUP
        Group name attribute
    
    

The configuration class in java maintains the configuration for the whole library. It is responsible for loading all the application configuration. It also loads the node configurations on demand. It is possible to create multiple node configurations and cycle through them. Or it is possible to load a node configuration dynamically. Users can also be loaded directly from the configuration. However password is required every time user tries to sign a post. If the application is built as a daemon it is suggested that this feature not be used and the signing happen in the client and the signed post sent to the configuration to be transmitted because of obvious security considerations. For more details about the API provided by the configuration class consult the JavaDoc generated documentation  